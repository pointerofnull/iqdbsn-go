package main

//go:generate sqlboiler --wipe sqlite3
import (
	"fmt"
	"log"
	"strconv"

	//"os"

	//"strconv"

	//. "github.com/ahmetb/go-linq"

	"github.com/jmoiron/sqlx"
	"github.com/jpillora/opts"
	_ "github.com/mattn/go-sqlite3"

	//"github.com/mkideal/cli"

	//	redditBooru "gitlab.com/pointerofnull/redditbooru-go/redditbooru"
	sauceNao "gitlab.com/pointerofnull/saucenao-go/saucenao"
	//"iqdbsn-go/models"
)

var db *sqlx.DB
var hydrusKey = "ec179dff885de8c661219759ad3782812632b8e5634430e8ee90a38f16bafd9a"

//Config is the various flags a user can pass
type Config struct {
	File  string `opts:"help=file to load"`
	Lines int    `opts:"help=number of lines to show"`
	Port  int    `opts:"help=port number for web ui to use"`
	Webui bool   `opts:"help=enable the webui (default false)"`
}

func main() {
	c := Config{Port: 5007}
	opts.Parse(&c).Run()
	db, err := sqlx.Open("sqlite3", fmt.Sprintf("%s?foreign_keys=on", sqliteFile))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%d, %t \n", c.Port, c.Webui)
	//initDB(db)
	defer db.Close()
	initDirectories()
	if c.Webui == true {
		startWEBUI()
	}
	//returnMatchesForImageID(2, db)
	//_, md5Hash := danbooruMD5("d05da062df837cd7affbadcc262526dee51b766d0ed1d7486f641951fa0538a7.jpg")
	//finalURL, md5Hash := buildURL(danbooruURL, "dbf9b4f1678050692efcb095c940fa2b698c5b35c466572d71801faaf17ef97f.jpg")
	//fmt.Printf("%s\n", finalURL)
	//resizeImage("7e593f314f6f1770674827ceb46e14313c0169172630c2f22be7a1462b844eec.jpg", md5Hash)
	//fmt.Printf("%s\n", md5Hash)
	//	insertTime := time.Now().Format(time.RFC3339)
	//	_, err = db.Exec(fmt.Sprintf("insert into files(md5sum, creation, updated) values('%s', '%s', '%s')", md5Hash, insertTime, insertTime))
	//	if err != nil {
	//		log.Fatal(err)
	//	}
	//addImage("d05da062df837cd7affbadcc262526dee51b766d0ed1d7486f641951fa0538a7.jpg", db)
	//https://anime-pictures.net/pictures/view_post/297175?lang=en
	//https://anime-pictures.net/pictures/view_post/594150?lang=en
	//animepicturesParser("anime-pictures.net", "https://anime-pictures.net/pictures/view_post/297175?lang=en", db)
	//var file = "d05da062df837cd7affbadcc262526dee51b766d0ed1d7486f641951fa0538a7.jpg"
	//fileHash, _ := hashFileMD5(file)
	//iqdbSearch := iqdbMatches{}
	//_, matches, _ := iqdbSearch.iqdbParser("https://iqdb.org/", "d05da062df837cd7affbadcc262526dee51b766d0ed1d7486f641951fa0538a7.jpg", fileHash, db)
	//matchResults := From(matches).Count()
	//fmt.Println("\n------------------------------------------------")
	//for i := range matches {
	//	fmt.Printf("%+v\n", matches[i])
	//}
	//if matchResults >= 1 {
	//	addIQDBMatches(matches, db)
	//}
	//zerochanParser("www.zerochan.net", "https://www.zerochan.net/2137641", db)
	//getHashes("dbf9b4f1678050692efcb095c940fa2b698c5b35c466572d71801faaf17ef97f.jpg")
	saucenaoClient := sauceNao.NewClient(nil)
	saucenaoResults, _, _ := saucenaoClient.ResultsService.ImageSearchByFile("", 5, "d05da062df837cd7affbadcc262526dee51b766d0ed1d7486f641951fa0538a7.png")
	for i := range saucenaoResults.Results {
		simu, _ := strconv.ParseFloat(saucenaoResults.Results[i].Header.Similarity, 64)
		if simu >= 90 {
			fmt.Printf("%+v\n", saucenaoResults.Results[i].Data.ExtURLS)
		}
	}
}

func buildURL(baseURL string, file string) (string, string) {
	md5Hash, err := hashFileMD5(file)
	if err != nil {
		log.Fatalf("Fatal error: %s\n", err)
		return "", ""
	}
	return fmt.Sprintf(baseURL, md5Hash), md5Hash
}
