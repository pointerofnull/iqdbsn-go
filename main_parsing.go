package main

import (
	"encoding/json"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	"github.com/goware/urlx"
	"github.com/jmoiron/sqlx"
	"github.com/tidwall/gjson"

	"github.com/gocolly/colly"
)

//var c *colly.Collector

func initParser(allowedDomains string) *colly.Collector {
	c := colly.NewCollector(
		colly.AllowedDomains(allowedDomains),
		colly.AllowURLRevisit(),
	)
	return c
}

func animepicturesParser(allowedDomains string, url string, db *sqlx.DB) {
	//var tags []string
	var NameSpace string
	//var SingleTags string
	tags := make(map[string]string)
	c := initParser(allowedDomains)
	// need to figure out best way to store the tags
	c.OnHTML("html", func(e *colly.HTMLElement) {
		ulChildren := e.DOM.Children()
		for i := range ulChildren.Nodes {
			single := ulChildren.Eq(i)
			tagsFnd := single.Find("ul[class='tags'] > li")

			for i := range tagsFnd.Nodes {
				nameSpace := tagsFnd.Eq(i).Prev().Not("li")
				for n := range nameSpace.Nodes {
					nameS := nameSpace.Eq(n)
					NameSpace = strings.TrimSpace(nameS.Text())
				}

				singleTag := tagsFnd.Eq(i).Find("a")
				tags[strings.TrimSpace(singleTag.Text())] = strings.TrimSpace(NameSpace)
				//addTag(strings.TrimSpace(singleTag.Text()), strings.TrimSpace(NameSpace), db)
				//fmt.Printf("Namespace: %s ---- Tag: %s\n", NameSpace, strings.TrimSpace(singleTag.Text()))
			}
		}
	})

	c.OnHTML("div[id='big_preview_cont'] a", func(e *colly.HTMLElement) {
		fmt.Printf("\nImage URL: %s\n", fmt.Sprintf("https://anime-pictures.net%s", strings.TrimSpace(e.Attr("href"))))
	})

	c.OnHTML("link[hreflang='en']", func(e *colly.HTMLElement) {
		fmt.Printf("Post URL: %s\n", strings.TrimSpace(e.Attr("href")))
	})

	c.Visit(url)
	addTags(tags, db)
	//fmt.Printf("len=%d cap=%d %v\n", len(tags), cap(tags), tags)
}

func zerochanParser(allowedDomains string, url string, db *sqlx.DB) {
	//var tags []string
	var NameSpace string
	var SingleTags string
	tags := make(map[string]string)
	c := initParser(allowedDomains)
	var ZerochanJSON = new(Zerochan)
	// need to figure out best way to store the tags
	c.OnHTML("html", func(e *colly.HTMLElement) {
		ulChildren := e.DOM.Children()
		for i := range ulChildren.Nodes {
			single := ulChildren.Eq(i)
			infoFnd := single.Find("div[id='content'] > script[type='application/ld+json']")
			for i := range infoFnd.Nodes {
				if gjson.Valid(infoFnd.Eq(i).Text()) {
					var resultByte []byte
					resultByte = []byte(infoFnd.Eq(i).Text())
					err := json.Unmarshal(resultByte, &ZerochanJSON)
					if err != nil {
						log.Fatalf("Failed to unmarshal provided json: %v", err)
					}
				}
			}
			namespaceFnd := single.Clone().Find("ul[id='tags'] > li")
			tagsFnd := single.Find("ul[id='tags'] > li")

			for i := range tagsFnd.Nodes {
				singleTag := tagsFnd.Eq(i).Find("a")
				SingleTags = strings.TrimSpace(singleTag.Text())
				//fmt.Printf("Tag: %s\n", strings.TrimSpace(singleTag.Text()))
				nameS := namespaceFnd.Eq(i).Clone().Children().Remove().End()
				NameSpace = nameS.Text()
				//fmt.Printf("Namespace: %s\n", strings.TrimSpace(NameSpace))
				tags[strings.TrimSpace(SingleTags)] = strings.TrimSpace(NameSpace)
				//addTag(strings.TrimSpace(singleTag.Text()), strings.TrimSpace(NameSpace), db)
				//fmt.Printf("NameSpace: %s, Tag: %s\n", NameSpace, SingleTags)
			}
		}
	})
	c.OnHTML("input[name='permalink']", func(e *colly.HTMLElement) {
		ZerochanJSON.PermaLink = strings.TrimSpace(e.Attr("value"))
	})
	c.Visit(url)
	fmt.Printf("DatePublished: %+v\n", ZerochanJSON)
	addTags(tags, db)
}

type iqdbResult struct {
	MatchURL      string
	ExtraMatchURL string
	Similarity    int
	ImgAlt        string
	ImageWidth    int
	ImageHeight   int
	Thumbnail     string
	Rating        int
	Status        int
	OrigImageID   int
}

type iqdbMatches struct {
	MatchNum map[int]*iqdbResult
}

const (
	unknownRating  = 0
	safeRating     = 1
	eroRating      = 2
	explicitRating = 3
	bestMatch      = 0
	possibleMatch  = 1
	otherMatch     = 2
)

//iqdbParser uploads file to iqdb.org and returns information about matches.
func (matches *iqdbMatches) iqdbParser(uri string, path string, md5SUM string, db *sqlx.DB) (string, map[int]*iqdbResult, bool) {
	//Make sure it isn't nil
	if matches.MatchNum == nil {
		matches.MatchNum = make(map[int]*iqdbResult)
	}

	var state = true
	var matchURL string
	var extraMatchURL string
	var simuPercent int
	var imgAlt string
	var iqdbThumb string
	var imageWidth int
	var imageHeight int
	var imageRating int
	//var matchType int
	//path = resizeImage(path)
	path, origImageID := addImage(path, db)
	//path = colly.SanitizeFileName(path)

	c := initParser("iqdb.org")
	extraParams := map[string]string{
		//"name":         "file",
		//"filename":     fileName,
		//"Content-Type": mime,
	}
	fileUpload, FileHeader, err := newfileUploadRequest("", extraParams, "file", path)
	if err != nil {
		log.Fatal("FileUpload:", err)
	}
	// Before making a request print "Visiting ..."
	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Posting image to", r.URL.String())
		r.Headers.Add("Content-Type", FileHeader)
	})
	c.OnResponse(func(resp *colly.Response) {
		//fmt.Printf("Status: %i\n", resp.StatusCode)
		//fmt.Printf("\nResponse%s\n", resp.Body)
	})
	c.OnHTML("html", func(e *colly.HTMLElement) {
		if e.Text == "" {
			fmt.Printf("No matches found")
			state = false
		}
		otherPostURL := e.DOM.Find("div[id='more1'] > div > div > table > tbody").Children()
		for i := range otherPostURL.Nodes {
			otherImageURL := otherPostURL.Eq(i).Find("td[class='image'] > a").Not("span[class='el']")
			for i := range otherImageURL.Nodes {
				val, exists := otherImageURL.Eq(i).Attr("href")
				if exists == true && val != "" {
					matchURL = val
				} else {
					matchURL = ""
				}
				/*extraResult := otherImageURL.Eq(i).Parent().Parent().Next().Is("span[class='el'] > a")
				if extraResult {
					extraImageURL := otherImageURL.Eq(i).Parent().Parent().Next().Find("span[class='el'] > a")
					val, exists2 := extraImageURL.Attr("href")
					if exists2 == true && val != "" {
						extraMatchURL = val
					} else {
						extraMatchURL = ""
					}

				}*/
			}

			simuResults := otherPostURL.Eq(i).Find("td:contains('similarity')")
			for i := range simuResults.Nodes {
				re := regexp.MustCompile(`\d\d`)
				simuParsed, _ := strconv.Atoi(re.FindString(simuResults.Eq(i).Text()))
				simuPercent = simuParsed
			}

			imageDementions := otherPostURL.Eq(i).Find("td:contains('×')")
			for i := range imageDementions.Nodes {
				imd := strings.Split(imageDementions.Eq(i).Text(), " ")

				widthHeight := strings.Split(imd[0], "×")

				imageW, _ := strconv.Atoi(widthHeight[0])
				imageWidth = imageW
				imageH, _ := strconv.Atoi(widthHeight[1])
				imageHeight = imageH

				ratingRE := regexp.MustCompile(`\w+`)
				imagerating := ratingRE.FindString(imd[1])
				switch imagerating {
				case "Safe":
					imageRating = safeRating
				case "Ero":
					imageRating = eroRating
				case "Explicit":
					imageRating = explicitRating
				default:
					imageRating = unknownRating
				}

			}

			imageAlt := otherPostURL.Eq(i).Find("td[class='image'] > a > img")
			for i := range imageAlt.Nodes {
				val, exists := imageAlt.Eq(i).Attr("alt")
				if exists == true && val != "" {
					imgAlt = val
				} else {
					imgAlt = ""
				}
			}

			imageThumbnail := otherPostURL.Eq(i).Find("td[class='image'] > a > img")
			for i := range imageThumbnail.Nodes {
				val, exists := imageThumbnail.Eq(i).Attr("src")
				if exists == true && val != "" {
					iqdbThumb = fmt.Sprintf("https://iqdb.org%s", val)
				} else {

				}
			}
			//matchType = otherMatch
			if imageWidth != 0 && imageHeight != 0 && simuPercent != 0 {
				urlNorm, _ := urlx.ParseWithDefaultScheme(matchURL, "https")
				matchNormal, errurl := urlx.Normalize(urlNorm)
				//fmt.Printf("Normal Match: %+v\n", matchNormal)
				if errurl != nil {
					fmt.Printf("Match URL: %+v\n", errurl)
				}
				extraURLNorm, _ := urlx.ParseWithDefaultScheme(extraMatchURL, "https")
				extraMatchNormal, errurl := urlx.Normalize(extraURLNorm)
				if errurl != nil {
					fmt.Printf("Extra Match URL: %+v\n", errurl)
					extraMatchNormal = ""
				}
				//fmt.Printf("MU: %s, SP: %v, IA: %s, IW: %v, IHL %v\n", matchURL, simuPercent, imgAlt, imageWidth, imageHeight)
				temp := &iqdbResult{matchNormal, extraMatchNormal, simuPercent, imgAlt, imageWidth, imageHeight, iqdbThumb, imageRating, otherMatch, origImageID}
				matches.MatchNum[i] = temp
				// zero these to precent issues
				imageWidth = 0
				imageHeight = 0
				simuPercent = 0
				//matches.MatchNum[postURLID] = &iqdbResult{matchURL, simuPercent, imageDemention, imgAlt, iqdbThumb}
			}

			state = true
		}

		bestPostURLClean := e.DOM.Find("div[id='more1']").Remove().End()
		bestPostURL := bestPostURLClean.Find("tbody").Children()
		bestResult := bestPostURL.Find("tr > th:contains('Best match')")
		if bestResult.Text() == "Best match" {
			for i := range bestPostURL.Nodes {
				imageURL := bestPostURL.Eq(i).Find("td[class='image'] > a").Not("span[class='el']")
				for i := range imageURL.Nodes {
					val, exists := imageURL.Eq(i).Attr("href")
					if exists == true && val != "" {
						matchURL = val
					} else {
						break
					}
					extraImageURL := imageURL.Eq(i).Parent().Parent().Next().Find("span[class='el'] > a")
					val, exists2 := extraImageURL.Attr("href")
					if exists2 == true && val != "" {
						extraMatchURL = val
					} else {
						extraMatchURL = ""
					}
				}

				simuResults := bestPostURL.Eq(i).Find("td:contains('similarity')")
				for i := range simuResults.Nodes {
					re := regexp.MustCompile(`\d\d`)
					simuParsed, _ := strconv.Atoi(re.FindString(simuResults.Eq(i).Text()))
					simuPercent = simuParsed
					//fmt.Printf("SimuPercentage: %+v\n", simuPercent)
				}

				imageDementions := bestPostURL.Eq(i).Find("tr > td:contains('×')")
				for i := range imageDementions.Nodes {
					val := imageDementions.Eq(i)
					imd := strings.Split(val.Text(), " ")

					widthHeight := strings.Split(imd[0], "×")

					imageW, _ := strconv.Atoi(widthHeight[0])
					imageWidth = imageW
					imageH, _ := strconv.Atoi(widthHeight[1])
					imageHeight = imageH

					ratingRE := regexp.MustCompile(`\w+`)
					imagerating := ratingRE.FindString(imd[1])
					switch imagerating {
					case "Safe":
						imageRating = safeRating
					case "Ero":
						imageRating = eroRating
					case "Explicit":
						imageRating = explicitRating
					default:
						imageRating = unknownRating
					}

				}

				imageAlt := bestPostURL.Eq(i).Find("td[class='image'] > a > img")
				for i := range imageAlt.Nodes {
					val, exists := imageAlt.Eq(i).Attr("alt")
					if exists == true && val != "" {
						imgAlt = val
					} else {
						break
					}
				}

				imageThumbnail := bestPostURL.Eq(i).Find("td[class='image'] > a > img")
				for i := range imageThumbnail.Nodes {
					val, exists := imageThumbnail.Eq(i).Attr("src")
					if exists == true && val != "" {
						iqdbThumb = fmt.Sprintf("https://iqdb.org%s", val)
					} else {
						break
					}
				}
				//matchType = bestMatch
				if imageWidth != 0 && imageHeight != 0 && simuPercent != 0 {
					urlNorm, _ := urlx.ParseWithDefaultScheme(matchURL, "https")
					matchNormal, errurl := urlx.Normalize(urlNorm)
					//fmt.Printf("Normal Match: %+v\n", matchNormal)
					if errurl != nil {
						fmt.Printf("Match URL: %+v\n", errurl)
					}
					extraURLNorm, _ := urlx.ParseWithDefaultScheme(extraMatchURL, "https")
					extraMatchNormal, errurl := urlx.Normalize(extraURLNorm)
					if errurl != nil {
						fmt.Printf("Extra Match URL: %+v\n", errurl)
						extraMatchNormal = ""
					}
					temp := &iqdbResult{matchNormal, extraMatchNormal, simuPercent, imgAlt, imageWidth, imageHeight, iqdbThumb, imageRating, bestMatch, origImageID}
					matches.MatchNum[i] = temp
					//make sure to zero these to prevent issues
					imageWidth = 0
					imageHeight = 0
					simuPercent = 0
				}
				//matches.MatchNum[postURLID] = &iqdbResult{matchURL, simuPercent, imageDemention, imgAlt, iqdbThumb}
				state = true
			}
		}
		additionalPostURLClean := e.DOM.Find("div[id='more1']").Remove().End()
		additionalPostURL := additionalPostURLClean.Find("div[id!='more1'] > div > table > tbody").Children() //.NotSelection(e.DOM.Find("th:contains('Your Image')").Parent().Next().Find("td[class='image']"))
		additionalResults := additionalPostURL.Find("tr > th:contains('Additional match')")
		if additionalResults.Text() == "Additional match" {
			for i := range additionalPostURL.Nodes {
				additionalImageURL := additionalPostURL.Eq(i).Find("td[class='image'] > a").Not("span[class='el']")
				for i := range additionalImageURL.Nodes {
					val, exists := additionalImageURL.Eq(i).Attr("href")
					if exists == true && val != "" {
						matchURL = val
					} else {
						break
					}
					extraImageURL := additionalImageURL.Eq(i).Parent().Parent().Next().Find("span[class='el'] > a")
					val, exists2 := extraImageURL.Attr("href")
					if exists2 == true && val != "" {
						extraMatchURL = val
					} else {
						extraMatchURL = ""
					}
				}

				simuResults := additionalPostURL.Eq(i).Find("td:contains('similarity')")
				for i := range simuResults.Nodes {
					re := regexp.MustCompile(`\d\d`)
					simuParsed, _ := strconv.Atoi(re.FindString(simuResults.Eq(i).Text()))
					simuPercent = simuParsed
				}

				imageDementions := additionalPostURL.Eq(i).Find("td:contains('×')")
				for i := range imageDementions.Nodes {
					val := imageDementions.Eq(i)
					imd := strings.Split(val.Text(), " ")

					widthHeight := strings.Split(imd[0], "×")

					imageW, _ := strconv.Atoi(widthHeight[0])
					imageWidth = imageW
					imageH, _ := strconv.Atoi(widthHeight[1])
					imageHeight = imageH

					ratingRE := regexp.MustCompile(`\w+`)
					imagerating := ratingRE.FindString(imd[1])
					switch imagerating {
					case "Safe":
						imageRating = safeRating
					case "Ero":
						imageRating = eroRating
					case "Explicit":
						imageRating = explicitRating
					default:
						imageRating = unknownRating
					}

				}

				imageAlt := additionalPostURL.Eq(i).Find("td[class='image'] > a > img")
				for i := range imageAlt.Nodes {
					val, exists := imageAlt.Eq(i).Attr("alt")
					if exists == true && val != "" {
						imgAlt = val
					} else {
						break
					}
				}

				imageThumbnail := additionalPostURL.Eq(i).Find("td[class='image'] > a > img")
				for i := range imageThumbnail.Nodes {
					val, exists := imageThumbnail.Eq(i).Attr("src")
					if exists == true && val != "" {
						iqdbThumb = fmt.Sprintf("https://iqdb.org%s", val)
					} else {
						break
					}
				}
				//matchType = possibleMatch
				if imageWidth != 0 && imageHeight != 0 && simuPercent != 0 {
					urlNorm, _ := urlx.ParseWithDefaultScheme(matchURL, "https")
					matchNormal, errurl := urlx.Normalize(urlNorm)
					//fmt.Printf("Normal Match: %+v\n", matchNormal)
					if errurl != nil {
						fmt.Printf("Match URL: %+v\n", errurl)
					}
					extraURLNorm, _ := urlx.ParseWithDefaultScheme(extraMatchURL, "https")
					extraMatchNormal, errurl := urlx.Normalize(extraURLNorm)
					if errurl != nil {
						fmt.Printf("Extra Match URL: %+v\n", errurl)
						extraMatchNormal = ""
					}
					temp := &iqdbResult{matchNormal, extraMatchNormal, simuPercent, imgAlt, imageWidth, imageHeight, iqdbThumb, imageRating, possibleMatch, origImageID}
					matches.MatchNum[i] = temp
					//zero these
					imageWidth = 0
					imageHeight = 0
					simuPercent = 0
				}
				//matches.MatchNum[postURLID] = &iqdbResult{matchURL, simuPercent, imageDemention, imgAlt, iqdbThumb}
			}
			state = true
		}
		state = false

	})
	//c.Request("POST", "http://10.10.1.20/upload.php", fileUpload, ctx*colly.Context)
	err = c.Request("POST", uri, fileUpload, nil, nil)
	if err != nil {
		log.Fatalf("Failed to POST file: %v", err)
	}

	c.Wait()
	/*for i := range matches.MatchNum {
		fmt.Println("-------------------------------------")
		fmt.Printf("MatchURL: %v\n", matches.MatchNum[i].MatchURL)
		fmt.Printf("ExtraMatchURL: %v\n", matches.MatchNum[i].ExtraMatchURL)
		fmt.Printf("Width: %v\n", matches.MatchNum[i].ImageWidth)
		fmt.Printf("Height: %v\n", matches.MatchNum[i].ImageHeight)
		fmt.Printf("Similarity: %v\n", matches.MatchNum[i].Similarity)
		fmt.Printf("ImgAlt: %v\n", matches.MatchNum[i].ImgAlt)
		fmt.Printf("Rating: %v\n", matches.MatchNum[i].Rating)
		fmt.Println("-------------------------------------")
	}*/
	return "", matches.MatchNum, state
}

func parserDelegator(uri string) {
	urlxD, _ := urlx.Parse(uri)
	fmt.Printf("Hostname: %s\n", urlxD.Hostname())
	switch urlxD.Hostname() {
	case "yande.re":
		re := regexp.MustCompile(`\d.*$`)
		postID, _ := strconv.Atoi(re.FindString(urlxD.Path))
		fmt.Printf("Yandere PostID: %+v\n", postID)
		//yandereClient := booruChan.NewClient(nil)
		//results, _, _ := yandereClient.YandereService.SearchByPostID(postID)
		//fmt.Printf("%+v\n", results[0])
	case "konachan.com":
		re := regexp.MustCompile(`\d.*$`)
		postID, _ := strconv.Atoi(re.FindString(urlxD.Path))
		fmt.Printf("KonaCom PostID: %+v\n", postID)
	case "danbooru.donmai.us":
		re := regexp.MustCompile(`\d.*$`)
		postID, _ := strconv.Atoi(re.FindString(urlxD.Path))
		fmt.Printf("Danbooru PostID: %+v\n", postID)
	case "konachan.net":
		re := regexp.MustCompile(`\d.*$`)
		postID, _ := strconv.Atoi(re.FindString(urlxD.Path))
		fmt.Printf("KonaNet PostID: %+v\n", postID)
	case "anime-pictures.net":
		fmt.Println("animepictures")
	}
}

//TODO:
//Make sure we check to see if no matches are found.
