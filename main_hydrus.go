package main

import (
	"encoding/hex"
	"fmt"
	"image"
	"io"
	"log"

	"github.com/devedge/imagehash"
	hydrusAPI "gitlab.com/pointerofnull/hydrusapi-go/hydrusapi"
	"gopkg.in/go-playground/validator.v9"
)

var validate *validator.Validate

func init() {

}

//GetThumbnailByID takes a id int and returns the thumbnail file, image type, and dhash
func GetThumbnailByID(id int64) (io.Reader, string, string) {
	validate = validator.New()
	hydrusQuery := hydrusAPI.NewClient(nil)

	if errs := validate.Var(id, "numeric"); errs == nil {
		id = id
	} else {
		log.Fatalf("Input is not a number: %x, Errors: %s", id, errs)
	}

	thumbnail, _, _ := hydrusQuery.FileService.ReturnThumbnailByID(hydrusKey, id)
	thumbImage, Info, err := image.Decode(thumbnail)
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
	dHashByte, err := imagehash.Dhash(thumbImage, 8)
	if err != nil {
		log.Fatalf("imagehash.Dhash() failed: %v", err)
		//return "", "", "", false
	}
	dHash := hex.EncodeToString(dHashByte)
	return thumbnail, dHash, Info
}

//GetImageFileByID takes a id int64 and returns the image file, image type, and dhash
func GetImageFileByID(id int64) (io.Reader, string, string) {
	validate = validator.New()
	hydrusQuery := hydrusAPI.NewClient(nil)

	if errs := validate.Var(id, "numeric"); errs == nil {
		id = id
	} else {
		log.Fatalf("Input is not a number: %x, Errors: %s", id, errs)
	}

	thumbnail, _, _ := hydrusQuery.FileService.ReturnFileByID(hydrusKey, id)
	thumbImage, Info, err := image.Decode(thumbnail)
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
	dHashByte, err := imagehash.Dhash(thumbImage, 8)
	if err != nil {
		log.Fatalf("imagehash.Dhash() failed: %v", err)
		//return "", "", "", false
	}
	dHash := hex.EncodeToString(dHashByte)
	return thumbnail, dHash, Info
}

//GetThumbnailByHash takes a sha256 hash and returns the thumbnail file, image type, and dhash
func GetThumbnailByHash(hash string) (io.Reader, string, string) {
	validate = validator.New()
	hydrusQuery := hydrusAPI.NewClient(nil)

	if errs := validate.Var(hash, "hexadecimal"); errs == nil {
		hash = hash
	} else {
		log.Fatalf("Input is not hex hash: %s, Errors: %s", hash, errs)
	}

	thumbnail, _, _ := hydrusQuery.FileService.ReturnThumbnailByHash(hydrusKey, hash)
	thumbImage, Info, err := image.Decode(thumbnail)
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
	dHashByte, err := imagehash.Dhash(thumbImage, 8)
	if err != nil {
		log.Fatalf("imagehash.Dhash() failed: %v", err)
		//return "", "", "", false
	}
	dHash := hex.EncodeToString(dHashByte)
	return thumbnail, dHash, Info
}

//GetImageFileByHash takes a sha256 hash and returns the image file, image type, and dhash
func GetImageFileByHash(hash string) (io.Reader, string, string) {
	validate = validator.New()
	hydrusQuery := hydrusAPI.NewClient(nil)

	if errs := validate.Var(hash, "hexadecimal"); errs == nil {
		hash = hash
	} else {
		log.Fatalf("Input is not hex hash: %s, Errors: %s", hash, errs)
	}

	thumbnail, _, _ := hydrusQuery.FileService.ReturnFileByHash(hydrusKey, hash)
	thumbImage, Info, err := image.Decode(thumbnail)
	if err != nil {
		fmt.Printf("%+v\n", err)
	}
	dHashByte, err := imagehash.Dhash(thumbImage, 8)
	if err != nil {
		log.Fatalf("imagehash.Dhash() failed: %v", err)
		//return "", "", "", false
	}
	dHash := hex.EncodeToString(dHashByte)
	return thumbnail, dHash, Info
}
