package main

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/h2non/filetype"
)

//initDirectories makes sure proper directories exist returns bool
func initDirectories() bool {
	cwd, _ := os.Getwd()
	thumbPath := filepath.FromSlash(fmt.Sprintf("%s/%s/%s", cwd, iqdbgoDir, thumbnailPath))
	err := os.MkdirAll(thumbPath, 755)
	if err != nil {
		log.Printf("Error creating directories: %s", err)
		return false
	}
	return true
}

//hashFileMD5 hashes the file contents of filePath and returns a md5hash in string form
//@param filePath
func hashFileMD5(filePath string) (string, error) {
	//Initialize variable returnMD5String now in case an error has to be returned
	var returnMD5String string

	//Open the passed argument and check for any error
	file, err := os.Open(filePath)
	if err != nil {
		return returnMD5String, err
	}

	//Tell the program to call the following function when the current function returns
	defer file.Close()

	//Open a new hash interface to write to
	hash := md5.New()

	//Copy the file in the hash interface and check for any error
	if _, err := io.Copy(hash, file); err != nil {
		return returnMD5String, err
	}

	//Get the 16 bytes hash
	hashInBytes := hash.Sum(nil)[:16]

	//Convert the bytes to a string
	returnMD5String = hex.EncodeToString(hashInBytes)

	return returnMD5String, nil

}

//hashFileSHA256 hashes the file contents of filePath and returns a sha256hash in string form.
//Returns empty string and error on failure
//@param filePath
func hashFileSHA256(filePath string) (string, error) {
	//Initialize variable returnMD5String now in case an error has to be returned
	var returnSHA256String string

	//Open the passed argument and check for any error
	file, err := os.Open(filePath)
	if err != nil {
		return returnSHA256String, err
	}

	//Tell the program to call the following function when the current function returns
	defer file.Close()

	//Open a new hash interface to write to
	hash := sha256.New()

	//Copy the file in the hash interface and check for any error
	if _, err := io.Copy(hash, file); err != nil {
		return returnSHA256String, err
	}

	//Get the 16 bytes hash
	//hashInBytes := hash.Sum(nil)[:16]

	//Convert the bytes to a string
	returnSHA256String = hex.EncodeToString(hash.Sum(nil))

	return returnSHA256String, nil

}

func generateFormData(path string) ([]byte, string) {
	f, _ := os.Open(path)
	defer f.Close()

	imgData, _ := ioutil.ReadAll(f)
	kind, _ := filetype.Match(imgData)
	if kind == filetype.Unknown {
		fmt.Println("Unknown file type")
	}
	return imgData, kind.MIME.Value
}
