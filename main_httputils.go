package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"mime/multipart"
	"os"
)

// Creates a new file upload http request with optional extra params
func newfileUploadRequest(uri string, params map[string]string, paramName, path string) (*bytes.Buffer, string, error) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("Error: ", err)
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println("Error: ", err)
	}
	fi, err := file.Stat()
	if err != nil {
		fmt.Println("Error: ", err)
	}
	file.Close()

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(paramName, fi.Name())
	if err != nil {
		fmt.Println("Error: ", err)
	}
	_, err = part.Write(fileContents)
	if err != nil {
		log.Fatalf("Error with part.Write: %v", err)
	}

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}
	err = writer.Close()
	if err != nil {
		fmt.Println("Error: ", err)
	}

	//var hdr http.Header
	//request, err := http.NewRequest("POST", uri, body)
	//http.Header
	//hdr.Add("Content-Type", writer.FormDataContentType())
	//request.Header.Add("Content-Type", writer.FormDataContentType())
	return body, writer.FormDataContentType(), err
}
