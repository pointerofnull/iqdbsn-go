package main

import (
	"encoding/hex"
	"fmt"
	"image"
	"log"
	"os"
	"strings"
	"time"

	. "github.com/ahmetb/go-linq"
	"github.com/devedge/imagehash"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

//dbConnect opens a connection
func dbConnect() {
	var dbConnectError error
	db, dbConnectError = sqlx.Open("sqlite3", fmt.Sprintf("%s?foreign_keys=on", sqliteFile))
	if dbConnectError != nil {
		log.Fatal(dbConnectError)
	}
	//defer the closing of database
	defer db.Close()
}
func initDB(db *sqlx.DB) {
	sqlStmt := `
	CREATE TABLE IF NOT EXISTS "files" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, "md5sum" TEXT NOT NULL UNIQUE, "creation" TEXT NOT NULL, "updated" TEXT NOT NULL, "thumbnail" BLOB);
	CREATE TABLE IF NOT EXISTS "tag" ("id" INTEGER NOT NULL PRIMARY KEY, "name" VARCHAR(255) NOT NULL UNIQUE, "namespace" VARCHAR(255));
	CREATE TABLE IF NOT EXISTS "imagemodel" ("id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, "checksum" VARCHAR(255) NOT NULL, "width" INTEGER NOT NULL, "height" INTEGER NOT NULL, "thumbnail" BLOB, "creation" TEXT NOT NULL);
	CREATE TABLE IF NOT EXISTS "namespace" ("id" INTEGER NOT NULL PRIMARY KEY, "name" VARCHAR(255) NOT NULL UNIQUE);`
	_, err := db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}
}

//addImage creates thumbnail of original file and adds information to database
func addImage(file string, db *sqlx.DB) (string, int) {
	_, err := db.Exec("PRAGMA foreign_keys = '1';")
	if err != nil {
		log.Fatalf("failed exec sqlite enable foreign keys: %v", err)
	}
	orighash, err := hashFileSHA256(file)
	origFile, err := os.Open(file)
	if err != nil {
		//return err
	}
	defer origFile.Close()
	origInfo, _, err := image.DecodeConfig(origFile)
	if err != nil {
		//return err
	}
	orighash, origDHash, origAHash, origHashStatus := getHashes(file)
	if origHashStatus == true {
		_, err = db.Exec(fmt.Sprintf("insert OR ignore into imageModel(sha256, width, height, path, aHash, dhash) values ('%s', '%v', '%v', '%s', '%s', '%s')", orighash, origInfo.Width, origInfo.Height, file, origAHash, origDHash))
		if err != nil {
			log.Fatalf("failed exec sqlite imageModel: %v", err)
		}
	}
	thumbFile := resizeImage(file)
	thumbnail, err := os.Open(thumbFile)
	if err != nil {
		//return err
	}
	defer thumbnail.Close()

	thumbnailInfo, _, err := image.DecodeConfig(thumbnail)
	thumbnailHash, thumbnailDHash, thumbnailAHash, thumbnailHashStatus := getHashes(file)
	if thumbnailHashStatus == true {
		_, err = db.Exec(fmt.Sprintf("insert OR ignore into thumbnailModel(sha256, width, height, path, aHash, dhash) values ('%s', '%v', '%v', '%s', '%s', '%s')", thumbnailHash, thumbnailInfo.Width, thumbnailInfo.Height, thumbFile, thumbnailAHash, thumbnailDHash))
		if err != nil {
			log.Fatalf("failed exec sqlite thumbnailModel: %v", err)
		}
	}
	var origShaSum int
	getErr := db.Get(&origShaSum, "SELECT id FROM imageModel WHERE sha256=$1", orighash)
	if getErr != nil {
		fmt.Printf("getErr id from sha256: %+v\n", getErr)
	}
	updateThumbnailRelationship(orighash, thumbnailHash, db)
	return thumbFile, origShaSum
}

func addTag(tag string, nameSpace string, db *sqlx.DB) {
	_, err := db.Exec(fmt.Sprintf("insert OR ignore into tag(tag, namespace) values ('%s', '%s')", strings.ToLower(tag), strings.ToLower(nameSpace)))
	if err != nil {
		log.Fatalf("failed exec sqlite: %v", err)
	}
}

//addTags adds a tag and its' namespace to db
func addTags(tags map[string]string, db *sqlx.DB) {
	tagsStmt, err := db.Prepare(`INSERT OR IGNORE INTO tag(tag, namespace) VALUES (?, ?)`)
	if err != nil {
		panic(err)
	}
	defer tagsStmt.Close()

	for tag, namespace := range tags {
		_, err2 := tagsStmt.Exec(tag, namespace)
		if err2 != nil {
			panic(err2)
		}
	}
}

//updateThumbnailRelationship links original file id to the thumbnail file id
//@params origHash string
//@params thumbnailHash string
//@params db *sqlx.DB
func updateThumbnailRelationship(origHash string, thumbnailHash string, db *sqlx.DB) {
	var origID int
	var thumbID int

	imageIDs, err1 := db.Query(fmt.Sprintf("SELECT imageModel.id, thumbnailModel.id FROM thumbnailModel INNER JOIN imageModel WHERE imageModel.sha256 = '%s' AND thumbnailModel.sha256 = '%s'", origHash, thumbnailHash))
	if err1 != nil {
		log.Fatalf("failed exec sqlite thumbRelationship: %v", err1)
	}

	imageIDs.Next()
	err := imageIDs.Scan(&origID, &thumbID)
	if err != nil {
		log.Fatalf("Failed to find imageID: %v", err)
	}
	imageIDs.Close()

	_, err = db.Exec(fmt.Sprintf("insert OR ignore into thumbnailRelationship(origID, thumbID) values ('%v', '%v')", origID, thumbID))
	if err != nil {
		log.Fatalf("failed exec sqlite thumbRelationship: %v", err)
	}
}

//getHashes returns A and D hashes of provided file
//Returns empty strings and false on failure
//@param filePath string path to file
func getHashes(filePath string) (sha256sum string, dHash string, aHash string, status bool) {
	sha256sum, err := hashFileSHA256(filePath)
	if err != nil {
		log.Fatalf("failed to calculate sha256sum of file: %v", err)
		return "", "", "", false
	}
	imageHash, err := imagehash.OpenImg(filePath)
	if err != nil {
		log.Fatalf("imagehash.OpenImg() failed: %v", err)
		return "", "", "", false
	}

	dHashByte, err := imagehash.Dhash(imageHash, 8)
	if err != nil {
		log.Fatalf("imagehash.Dhash() failed: %v", err)
		return "", "", "", false
	}
	dHash = hex.EncodeToString(dHashByte)

	aHashByte, err := imagehash.Ahash(imageHash, 12)
	if err != nil {
		log.Fatalf("imagehash.Ahash() failed: %v", err)
		return "", "", "", false
	}
	aHash = hex.EncodeToString(aHashByte)

	return sha256sum, dHash, aHash, status
}

//addIQDBMatches adds matched image info from iqdb to db
func addIQDBMatches(matches map[int]*iqdbResult, db *sqlx.DB) {
	matchStmt, err := db.Prepare(`INSERT OR IGNORE INTO match(href, thumb, rating, imgAlt, height, width, extrahref) VALUES (?, ?, ?, ?, ?, ?, ?)`)
	if err != nil {
		panic(err)
	}
	defer matchStmt.Close()

	for i := range matches {
		href := matches[i].MatchURL
		thumb := matches[i].Thumbnail
		rating := matches[i].Rating
		imgAlt := matches[i].ImgAlt
		height := matches[i].ImageHeight
		width := matches[i].ImageWidth
		extrahref := matches[i].ExtraMatchURL
		_, err2 := matchStmt.Exec(href, thumb, rating, imgAlt, height, width, extrahref)
		if err2 != nil {
			panic(err2)
		}

		matchID, err1 := db.Query(fmt.Sprintf("SELECT id FROM match WHERE href = '%s'", matches[i].MatchURL))
		if err1 != nil {
			log.Fatalf("failed exec sqlite thumbRelationship: %v", err1)
		}
		var matchid int
		matchID.Next()
		err := matchID.Scan(&matchid)
		if err != nil {
			log.Fatalf("Failed to find matchID: %v", err)
		}
		matchID.Close()
		addImageMatch(matches[i], matchid, db)
		updateMatchRelationship(matches[i].OrigImageID, matchid, db)
	}
}

//addImageMatch adds an entry to imageMatch for matched image used with addImageMatches
func addImageMatch(matches *iqdbResult, matchID int, db *sqlx.DB) {
	imageMatchStmt, err4 := db.Prepare(`INSERT OR IGNORE INTO imageMatch(matchID, similarity, status, createdAt) VALUES (?, ?, ?, ?)`)
	if err4 != nil {
		panic(err4)
	}
	defer imageMatchStmt.Close()

	_, err := imageMatchStmt.Exec(matchID, matches.Similarity, matches.Status, time.Now())
	if err != nil {
		panic(err)
	}
}

//updateMatchRelationship links imageID with found match(matchID)
func updateMatchRelationship(imageID int, matchID int, db *sqlx.DB) {
	imageMatchRelationshipStmt, err4 := db.Prepare(`INSERT OR IGNORE INTO imageMatchRelationship(imageID, matchResultID) VALUES (?, ?)`)
	if err4 != nil {
		panic(err4)
	}
	defer imageMatchRelationshipStmt.Close()

	_, err := imageMatchRelationshipStmt.Exec(imageID, matchID)
	if err != nil {
		panic(err)
	}
}

//returnMatchByID returns information on matchID
func returnMatchByID(matchID uint32) *imageMatch {
	db, err := sqlx.Open("sqlite3", fmt.Sprintf("%s?foreign_keys=on", sqliteFile))
	if err != nil {
		log.Fatal(err)
	}
	//initDB(db)
	defer db.Close()
	err2 := db.Ping()
	if err2 != nil {
		fmt.Printf("Ping Error%+v\n", err)
	}
	match := imageMatch{}
	getErr := db.Get(&match, "SELECT * FROM match WHERE id=$1", matchID)
	if getErr != nil {
		fmt.Printf("getErr: %+v\n", getErr)
	}
	return &match
}

//returnMatchByHash returns information on sha256
func returnMatchByHash(matchID uint32) *imageMatch {
	db, err := sqlx.Open("sqlite3", fmt.Sprintf("%s?foreign_keys=on", sqliteFile))
	if err != nil {
		log.Fatal(err)
	}
	//initDB(db)
	defer db.Close()
	err2 := db.Ping()
	if err2 != nil {
		fmt.Printf("Ping Error%+v\n", err)
	}
	match := imageMatch{}
	getErr := db.Get(&match, "SELECT * FROM match WHERE id=$1", matchID)
	if getErr != nil {
		fmt.Printf("getErr: %+v\n", getErr)
	}
	return &match
}

//returnMatchByDHash returns information on dhash
func returnMatchByDHash(matchID uint32) *imageMatch {
	db, err := sqlx.Open("sqlite3", fmt.Sprintf("%s?foreign_keys=on", sqliteFile))
	if err != nil {
		log.Fatal(err)
	}
	//initDB(db)
	defer db.Close()
	err2 := db.Ping()
	if err2 != nil {
		fmt.Printf("Ping Error%+v\n", err2)
	}
	match := imageMatch{}
	getErr := db.Get(&match, "SELECT * FROM match WHERE id=$1", matchID)
	if getErr != nil {
		fmt.Printf("getErr: %+v\n", getErr)
	}
	return &match
}

//updateHashRelationship links thumbnail and original image hashes in table
func updateHashRelationship(imageID int, thumbnailID int, db *sqlx.DB) {

	pingErr := db.Ping()
	if pingErr != nil {
		fmt.Printf("Ping Error%+v\n", pingErr)
	}
	//db.Get("SELECT (aHash, dHash) FROM")
	// /db.Get()

}

func returnMatchesForImageID(imageID int, db *sqlx.DB) {
	pingErr := db.Ping()
	if pingErr != nil {
		fmt.Printf("Ping Error%+v\n", pingErr)
	}
	var matchResultIDs []int

	rows, _ := db.Queryx("SELECT matchResultID FROM imageMatchRelationship WHERE imageID=$1", imageID)
	defer rows.Close()
	for rows.Next() {
		var matchResultID int
		err := rows.Scan(&matchResultID)
		if err != nil {
			log.Fatalln(err)
		}
		matchResultIDs = append(matchResultIDs, matchResultID)
	}

	From(matchResultIDs).Distinct().ForEach(func(matchid interface{}) {
		var matchHREF string
		err := db.Get(&matchHREF, "SELECT href FROM match WHERE id=$1", matchid)
		if err != nil {
			log.Fatalf("href%v", err)
		}
		parserDelegator(matchHREF)
		fmt.Printf("MatchID: %+v href: %+v\n", matchid, matchHREF)
	})
}
