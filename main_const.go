package main

const (
	konachanNetURL  = "https://konachan.net/post.json?limit=1&tags=md5:%s"
	konachanComURL  = "https://konachan.com/post.json?limit=1&tags=md5:%s"
	danbooruURL     = "https://danbooru.donmai.us/post/index.json?limit=1&tags=md5:%s" //"https://danbooru.donmai.us/post.json?limit=1&tags=md5:%s"
	yandereURL      = "https://yande.re/post.json?limit=1&tags=md5:%s"
	saucenaoURL     = "https://saucenao.com/search.php?db=999&output_type=2&testmode=1&numres=16&url=http%3A%2F%2Fsaucenao.com%2Fimages%2Fstatic%2Fbanner.gif" //POST file
	iqdbURL         = "https://iqdb.org/?url=http://saucenao.com/userdata/8BOAxMMJr.jpg.png"                                                                   // POST File
	safebooruorgURL = "https://safebooru.org/index.php?page=post&s=list&tags=md5:%s"
	safeboorudanURL = "https://safebooru.donmai.us/posts.json?limit=1&tags=md5:%s"
	rule34pahealURL = "https://rule34.paheal.net/post/list/md5=%s/1"
	rule34hentaiURL = "https://rule34hentai.net/post/list/md5=%s/1"
	//rule34xxxURL    = "https://rule34.xxx/index.php?page=post&s=list&tags=md5:%s"

	sqliteFile         = "./foo.db"
	iqdbgoDir          = "iqdbgo"
	thumbnailPath      = "thumbnails"
	applicationName    = "IQDB-GO"
	applicationVersion = "0.1"
)

type imageMatch struct {
	ID            int    `db:"id"`
	MatchURL      string `db:"href"`
	ExtraMatchURL string `db:"extrahref"`
	ImgAlt        string `db:"imgAlt"`
	ImageWidth    int    `db:"width"`
	ImageHeight   int    `db:"height"`
	Thumbnail     string `db:"thumb"`
	Rating        int    `db:"rating"`
}

//Zerochan contains information gotten from zerochan page
type Zerochan struct {
	Context        string `json:"@context"`
	Type           string `json:"@type"`
	Author         string `json:"author"`
	ContentURL     string `json:"contentUrl"`
	Thumbnail      string `json:"thumbnail"`
	EncodingFormat string `json:"encodingFormat"`
	DatePublished  string `json:"datePublished"`
	Description    string `json:"description"`
	Name           string `json:"name"`
	Width          string `json:"width"`
	Height         string `json:"height"`
	ContentSize    string `json:"contentSize"`
	PermaLink      string `json:"permalink,omitempty"`
}
