// Code generated by SQLBoiler (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"github.com/volatiletech/sqlboiler/queries/qmhelper"
	"github.com/volatiletech/sqlboiler/strmangle"
)

// Matchtagrelationship is an object representing the database table.
type Matchtagrelationship struct {
	ID      int64 `boil:"id" json:"id" toml:"id" yaml:"id"`
	MatchID int64 `boil:"match_id" json:"match_id" toml:"match_id" yaml:"match_id"`
	TagID   int64 `boil:"tag_id" json:"tag_id" toml:"tag_id" yaml:"tag_id"`

	R *matchtagrelationshipR `boil:"-" json:"-" toml:"-" yaml:"-"`
	L matchtagrelationshipL  `boil:"-" json:"-" toml:"-" yaml:"-"`
}

var MatchtagrelationshipColumns = struct {
	ID      string
	MatchID string
	TagID   string
}{
	ID:      "id",
	MatchID: "match_id",
	TagID:   "tag_id",
}

// Generated where

var MatchtagrelationshipWhere = struct {
	ID      whereHelperint64
	MatchID whereHelperint64
	TagID   whereHelperint64
}{
	ID:      whereHelperint64{field: "\"matchtagrelationship\".\"id\""},
	MatchID: whereHelperint64{field: "\"matchtagrelationship\".\"match_id\""},
	TagID:   whereHelperint64{field: "\"matchtagrelationship\".\"tag_id\""},
}

// MatchtagrelationshipRels is where relationship names are stored.
var MatchtagrelationshipRels = struct {
	Tag   string
	Match string
}{
	Tag:   "Tag",
	Match: "Match",
}

// matchtagrelationshipR is where relationships are stored.
type matchtagrelationshipR struct {
	Tag   *Tag
	Match *Match
}

// NewStruct creates a new relationship struct
func (*matchtagrelationshipR) NewStruct() *matchtagrelationshipR {
	return &matchtagrelationshipR{}
}

// matchtagrelationshipL is where Load methods for each relationship are stored.
type matchtagrelationshipL struct{}

var (
	matchtagrelationshipAllColumns            = []string{"id", "match_id", "tag_id"}
	matchtagrelationshipColumnsWithoutDefault = []string{"match_id", "tag_id"}
	matchtagrelationshipColumnsWithDefault    = []string{"id"}
	matchtagrelationshipPrimaryKeyColumns     = []string{"id"}
)

type (
	// MatchtagrelationshipSlice is an alias for a slice of pointers to Matchtagrelationship.
	// This should generally be used opposed to []Matchtagrelationship.
	MatchtagrelationshipSlice []*Matchtagrelationship
	// MatchtagrelationshipHook is the signature for custom Matchtagrelationship hook methods
	MatchtagrelationshipHook func(context.Context, boil.ContextExecutor, *Matchtagrelationship) error

	matchtagrelationshipQuery struct {
		*queries.Query
	}
)

// Cache for insert, update and upsert
var (
	matchtagrelationshipType                 = reflect.TypeOf(&Matchtagrelationship{})
	matchtagrelationshipMapping              = queries.MakeStructMapping(matchtagrelationshipType)
	matchtagrelationshipPrimaryKeyMapping, _ = queries.BindMapping(matchtagrelationshipType, matchtagrelationshipMapping, matchtagrelationshipPrimaryKeyColumns)
	matchtagrelationshipInsertCacheMut       sync.RWMutex
	matchtagrelationshipInsertCache          = make(map[string]insertCache)
	matchtagrelationshipUpdateCacheMut       sync.RWMutex
	matchtagrelationshipUpdateCache          = make(map[string]updateCache)
	matchtagrelationshipUpsertCacheMut       sync.RWMutex
	matchtagrelationshipUpsertCache          = make(map[string]insertCache)
)

var (
	// Force time package dependency for automated UpdatedAt/CreatedAt.
	_ = time.Second
	// Force qmhelper dependency for where clause generation (which doesn't
	// always happen)
	_ = qmhelper.Where
)

var matchtagrelationshipBeforeInsertHooks []MatchtagrelationshipHook
var matchtagrelationshipBeforeUpdateHooks []MatchtagrelationshipHook
var matchtagrelationshipBeforeDeleteHooks []MatchtagrelationshipHook
var matchtagrelationshipBeforeUpsertHooks []MatchtagrelationshipHook

var matchtagrelationshipAfterInsertHooks []MatchtagrelationshipHook
var matchtagrelationshipAfterSelectHooks []MatchtagrelationshipHook
var matchtagrelationshipAfterUpdateHooks []MatchtagrelationshipHook
var matchtagrelationshipAfterDeleteHooks []MatchtagrelationshipHook
var matchtagrelationshipAfterUpsertHooks []MatchtagrelationshipHook

// doBeforeInsertHooks executes all "before insert" hooks.
func (o *Matchtagrelationship) doBeforeInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipBeforeInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpdateHooks executes all "before Update" hooks.
func (o *Matchtagrelationship) doBeforeUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipBeforeUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeDeleteHooks executes all "before Delete" hooks.
func (o *Matchtagrelationship) doBeforeDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipBeforeDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doBeforeUpsertHooks executes all "before Upsert" hooks.
func (o *Matchtagrelationship) doBeforeUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipBeforeUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterInsertHooks executes all "after Insert" hooks.
func (o *Matchtagrelationship) doAfterInsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipAfterInsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterSelectHooks executes all "after Select" hooks.
func (o *Matchtagrelationship) doAfterSelectHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipAfterSelectHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpdateHooks executes all "after Update" hooks.
func (o *Matchtagrelationship) doAfterUpdateHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipAfterUpdateHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterDeleteHooks executes all "after Delete" hooks.
func (o *Matchtagrelationship) doAfterDeleteHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipAfterDeleteHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// doAfterUpsertHooks executes all "after Upsert" hooks.
func (o *Matchtagrelationship) doAfterUpsertHooks(ctx context.Context, exec boil.ContextExecutor) (err error) {
	if boil.HooksAreSkipped(ctx) {
		return nil
	}

	for _, hook := range matchtagrelationshipAfterUpsertHooks {
		if err := hook(ctx, exec, o); err != nil {
			return err
		}
	}

	return nil
}

// AddMatchtagrelationshipHook registers your hook function for all future operations.
func AddMatchtagrelationshipHook(hookPoint boil.HookPoint, matchtagrelationshipHook MatchtagrelationshipHook) {
	switch hookPoint {
	case boil.BeforeInsertHook:
		matchtagrelationshipBeforeInsertHooks = append(matchtagrelationshipBeforeInsertHooks, matchtagrelationshipHook)
	case boil.BeforeUpdateHook:
		matchtagrelationshipBeforeUpdateHooks = append(matchtagrelationshipBeforeUpdateHooks, matchtagrelationshipHook)
	case boil.BeforeDeleteHook:
		matchtagrelationshipBeforeDeleteHooks = append(matchtagrelationshipBeforeDeleteHooks, matchtagrelationshipHook)
	case boil.BeforeUpsertHook:
		matchtagrelationshipBeforeUpsertHooks = append(matchtagrelationshipBeforeUpsertHooks, matchtagrelationshipHook)
	case boil.AfterInsertHook:
		matchtagrelationshipAfterInsertHooks = append(matchtagrelationshipAfterInsertHooks, matchtagrelationshipHook)
	case boil.AfterSelectHook:
		matchtagrelationshipAfterSelectHooks = append(matchtagrelationshipAfterSelectHooks, matchtagrelationshipHook)
	case boil.AfterUpdateHook:
		matchtagrelationshipAfterUpdateHooks = append(matchtagrelationshipAfterUpdateHooks, matchtagrelationshipHook)
	case boil.AfterDeleteHook:
		matchtagrelationshipAfterDeleteHooks = append(matchtagrelationshipAfterDeleteHooks, matchtagrelationshipHook)
	case boil.AfterUpsertHook:
		matchtagrelationshipAfterUpsertHooks = append(matchtagrelationshipAfterUpsertHooks, matchtagrelationshipHook)
	}
}

// One returns a single matchtagrelationship record from the query.
func (q matchtagrelationshipQuery) One(ctx context.Context, exec boil.ContextExecutor) (*Matchtagrelationship, error) {
	o := &Matchtagrelationship{}

	queries.SetLimit(q.Query, 1)

	err := q.Bind(ctx, exec, o)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: failed to execute a one query for matchtagrelationship")
	}

	if err := o.doAfterSelectHooks(ctx, exec); err != nil {
		return o, err
	}

	return o, nil
}

// All returns all Matchtagrelationship records from the query.
func (q matchtagrelationshipQuery) All(ctx context.Context, exec boil.ContextExecutor) (MatchtagrelationshipSlice, error) {
	var o []*Matchtagrelationship

	err := q.Bind(ctx, exec, &o)
	if err != nil {
		return nil, errors.Wrap(err, "models: failed to assign all query results to Matchtagrelationship slice")
	}

	if len(matchtagrelationshipAfterSelectHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterSelectHooks(ctx, exec); err != nil {
				return o, err
			}
		}
	}

	return o, nil
}

// Count returns the count of all Matchtagrelationship records in the query.
func (q matchtagrelationshipQuery) Count(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to count matchtagrelationship rows")
	}

	return count, nil
}

// Exists checks if the row exists in the table.
func (q matchtagrelationshipQuery) Exists(ctx context.Context, exec boil.ContextExecutor) (bool, error) {
	var count int64

	queries.SetSelect(q.Query, nil)
	queries.SetCount(q.Query)
	queries.SetLimit(q.Query, 1)

	err := q.Query.QueryRowContext(ctx, exec).Scan(&count)
	if err != nil {
		return false, errors.Wrap(err, "models: failed to check if matchtagrelationship exists")
	}

	return count > 0, nil
}

// Tag pointed to by the foreign key.
func (o *Matchtagrelationship) Tag(mods ...qm.QueryMod) tagQuery {
	queryMods := []qm.QueryMod{
		qm.Where("id=?", o.TagID),
	}

	queryMods = append(queryMods, mods...)

	query := Tags(queryMods...)
	queries.SetFrom(query.Query, "\"tag\"")

	return query
}

// Match pointed to by the foreign key.
func (o *Matchtagrelationship) Match(mods ...qm.QueryMod) matchQuery {
	queryMods := []qm.QueryMod{
		qm.Where("id=?", o.MatchID),
	}

	queryMods = append(queryMods, mods...)

	query := Matches(queryMods...)
	queries.SetFrom(query.Query, "\"match\"")

	return query
}

// LoadTag allows an eager lookup of values, cached into the
// loaded structs of the objects. This is for an N-1 relationship.
func (matchtagrelationshipL) LoadTag(ctx context.Context, e boil.ContextExecutor, singular bool, maybeMatchtagrelationship interface{}, mods queries.Applicator) error {
	var slice []*Matchtagrelationship
	var object *Matchtagrelationship

	if singular {
		object = maybeMatchtagrelationship.(*Matchtagrelationship)
	} else {
		slice = *maybeMatchtagrelationship.(*[]*Matchtagrelationship)
	}

	args := make([]interface{}, 0, 1)
	if singular {
		if object.R == nil {
			object.R = &matchtagrelationshipR{}
		}
		args = append(args, object.TagID)

	} else {
	Outer:
		for _, obj := range slice {
			if obj.R == nil {
				obj.R = &matchtagrelationshipR{}
			}

			for _, a := range args {
				if a == obj.TagID {
					continue Outer
				}
			}

			args = append(args, obj.TagID)

		}
	}

	if len(args) == 0 {
		return nil
	}

	query := NewQuery(qm.From(`tag`), qm.WhereIn(`id in ?`, args...))
	if mods != nil {
		mods.Apply(query)
	}

	results, err := query.QueryContext(ctx, e)
	if err != nil {
		return errors.Wrap(err, "failed to eager load Tag")
	}

	var resultSlice []*Tag
	if err = queries.Bind(results, &resultSlice); err != nil {
		return errors.Wrap(err, "failed to bind eager loaded slice Tag")
	}

	if err = results.Close(); err != nil {
		return errors.Wrap(err, "failed to close results of eager load for tag")
	}
	if err = results.Err(); err != nil {
		return errors.Wrap(err, "error occurred during iteration of eager loaded relations for tag")
	}

	if len(matchtagrelationshipAfterSelectHooks) != 0 {
		for _, obj := range resultSlice {
			if err := obj.doAfterSelectHooks(ctx, e); err != nil {
				return err
			}
		}
	}

	if len(resultSlice) == 0 {
		return nil
	}

	if singular {
		foreign := resultSlice[0]
		object.R.Tag = foreign
		if foreign.R == nil {
			foreign.R = &tagR{}
		}
		foreign.R.Matchtagrelationships = append(foreign.R.Matchtagrelationships, object)
		return nil
	}

	for _, local := range slice {
		for _, foreign := range resultSlice {
			if local.TagID == foreign.ID {
				local.R.Tag = foreign
				if foreign.R == nil {
					foreign.R = &tagR{}
				}
				foreign.R.Matchtagrelationships = append(foreign.R.Matchtagrelationships, local)
				break
			}
		}
	}

	return nil
}

// LoadMatch allows an eager lookup of values, cached into the
// loaded structs of the objects. This is for an N-1 relationship.
func (matchtagrelationshipL) LoadMatch(ctx context.Context, e boil.ContextExecutor, singular bool, maybeMatchtagrelationship interface{}, mods queries.Applicator) error {
	var slice []*Matchtagrelationship
	var object *Matchtagrelationship

	if singular {
		object = maybeMatchtagrelationship.(*Matchtagrelationship)
	} else {
		slice = *maybeMatchtagrelationship.(*[]*Matchtagrelationship)
	}

	args := make([]interface{}, 0, 1)
	if singular {
		if object.R == nil {
			object.R = &matchtagrelationshipR{}
		}
		args = append(args, object.MatchID)

	} else {
	Outer:
		for _, obj := range slice {
			if obj.R == nil {
				obj.R = &matchtagrelationshipR{}
			}

			for _, a := range args {
				if a == obj.MatchID {
					continue Outer
				}
			}

			args = append(args, obj.MatchID)

		}
	}

	if len(args) == 0 {
		return nil
	}

	query := NewQuery(qm.From(`match`), qm.WhereIn(`id in ?`, args...))
	if mods != nil {
		mods.Apply(query)
	}

	results, err := query.QueryContext(ctx, e)
	if err != nil {
		return errors.Wrap(err, "failed to eager load Match")
	}

	var resultSlice []*Match
	if err = queries.Bind(results, &resultSlice); err != nil {
		return errors.Wrap(err, "failed to bind eager loaded slice Match")
	}

	if err = results.Close(); err != nil {
		return errors.Wrap(err, "failed to close results of eager load for match")
	}
	if err = results.Err(); err != nil {
		return errors.Wrap(err, "error occurred during iteration of eager loaded relations for match")
	}

	if len(matchtagrelationshipAfterSelectHooks) != 0 {
		for _, obj := range resultSlice {
			if err := obj.doAfterSelectHooks(ctx, e); err != nil {
				return err
			}
		}
	}

	if len(resultSlice) == 0 {
		return nil
	}

	if singular {
		foreign := resultSlice[0]
		object.R.Match = foreign
		if foreign.R == nil {
			foreign.R = &matchR{}
		}
		foreign.R.Matchtagrelationships = append(foreign.R.Matchtagrelationships, object)
		return nil
	}

	for _, local := range slice {
		for _, foreign := range resultSlice {
			if local.MatchID == foreign.ID {
				local.R.Match = foreign
				if foreign.R == nil {
					foreign.R = &matchR{}
				}
				foreign.R.Matchtagrelationships = append(foreign.R.Matchtagrelationships, local)
				break
			}
		}
	}

	return nil
}

// SetTag of the matchtagrelationship to the related item.
// Sets o.R.Tag to related.
// Adds o to related.R.Matchtagrelationships.
func (o *Matchtagrelationship) SetTag(ctx context.Context, exec boil.ContextExecutor, insert bool, related *Tag) error {
	var err error
	if insert {
		if err = related.Insert(ctx, exec, boil.Infer()); err != nil {
			return errors.Wrap(err, "failed to insert into foreign table")
		}
	}

	updateQuery := fmt.Sprintf(
		"UPDATE \"matchtagrelationship\" SET %s WHERE %s",
		strmangle.SetParamNames("\"", "\"", 0, []string{"tag_id"}),
		strmangle.WhereClause("\"", "\"", 0, matchtagrelationshipPrimaryKeyColumns),
	)
	values := []interface{}{related.ID, o.ID}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, updateQuery)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	if _, err = exec.ExecContext(ctx, updateQuery, values...); err != nil {
		return errors.Wrap(err, "failed to update local table")
	}

	o.TagID = related.ID
	if o.R == nil {
		o.R = &matchtagrelationshipR{
			Tag: related,
		}
	} else {
		o.R.Tag = related
	}

	if related.R == nil {
		related.R = &tagR{
			Matchtagrelationships: MatchtagrelationshipSlice{o},
		}
	} else {
		related.R.Matchtagrelationships = append(related.R.Matchtagrelationships, o)
	}

	return nil
}

// SetMatch of the matchtagrelationship to the related item.
// Sets o.R.Match to related.
// Adds o to related.R.Matchtagrelationships.
func (o *Matchtagrelationship) SetMatch(ctx context.Context, exec boil.ContextExecutor, insert bool, related *Match) error {
	var err error
	if insert {
		if err = related.Insert(ctx, exec, boil.Infer()); err != nil {
			return errors.Wrap(err, "failed to insert into foreign table")
		}
	}

	updateQuery := fmt.Sprintf(
		"UPDATE \"matchtagrelationship\" SET %s WHERE %s",
		strmangle.SetParamNames("\"", "\"", 0, []string{"match_id"}),
		strmangle.WhereClause("\"", "\"", 0, matchtagrelationshipPrimaryKeyColumns),
	)
	values := []interface{}{related.ID, o.ID}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, updateQuery)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	if _, err = exec.ExecContext(ctx, updateQuery, values...); err != nil {
		return errors.Wrap(err, "failed to update local table")
	}

	o.MatchID = related.ID
	if o.R == nil {
		o.R = &matchtagrelationshipR{
			Match: related,
		}
	} else {
		o.R.Match = related
	}

	if related.R == nil {
		related.R = &matchR{
			Matchtagrelationships: MatchtagrelationshipSlice{o},
		}
	} else {
		related.R.Matchtagrelationships = append(related.R.Matchtagrelationships, o)
	}

	return nil
}

// Matchtagrelationships retrieves all the records using an executor.
func Matchtagrelationships(mods ...qm.QueryMod) matchtagrelationshipQuery {
	mods = append(mods, qm.From("\"matchtagrelationship\""))
	return matchtagrelationshipQuery{NewQuery(mods...)}
}

// FindMatchtagrelationship retrieves a single record by ID with an executor.
// If selectCols is empty Find will return all columns.
func FindMatchtagrelationship(ctx context.Context, exec boil.ContextExecutor, iD int64, selectCols ...string) (*Matchtagrelationship, error) {
	matchtagrelationshipObj := &Matchtagrelationship{}

	sel := "*"
	if len(selectCols) > 0 {
		sel = strings.Join(strmangle.IdentQuoteSlice(dialect.LQ, dialect.RQ, selectCols), ",")
	}
	query := fmt.Sprintf(
		"select %s from \"matchtagrelationship\" where \"id\"=?", sel,
	)

	q := queries.Raw(query, iD)

	err := q.Bind(ctx, exec, matchtagrelationshipObj)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, sql.ErrNoRows
		}
		return nil, errors.Wrap(err, "models: unable to select from matchtagrelationship")
	}

	return matchtagrelationshipObj, nil
}

// Insert a single record using an executor.
// See boil.Columns.InsertColumnSet documentation to understand column list inference for inserts.
func (o *Matchtagrelationship) Insert(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) error {
	if o == nil {
		return errors.New("models: no matchtagrelationship provided for insertion")
	}

	var err error

	if err := o.doBeforeInsertHooks(ctx, exec); err != nil {
		return err
	}

	nzDefaults := queries.NonZeroDefaultSet(matchtagrelationshipColumnsWithDefault, o)

	key := makeCacheKey(columns, nzDefaults)
	matchtagrelationshipInsertCacheMut.RLock()
	cache, cached := matchtagrelationshipInsertCache[key]
	matchtagrelationshipInsertCacheMut.RUnlock()

	if !cached {
		wl, returnColumns := columns.InsertColumnSet(
			matchtagrelationshipAllColumns,
			matchtagrelationshipColumnsWithDefault,
			matchtagrelationshipColumnsWithoutDefault,
			nzDefaults,
		)

		cache.valueMapping, err = queries.BindMapping(matchtagrelationshipType, matchtagrelationshipMapping, wl)
		if err != nil {
			return err
		}
		cache.retMapping, err = queries.BindMapping(matchtagrelationshipType, matchtagrelationshipMapping, returnColumns)
		if err != nil {
			return err
		}
		if len(wl) != 0 {
			cache.query = fmt.Sprintf("INSERT INTO \"matchtagrelationship\" (\"%s\") %%sVALUES (%s)%%s", strings.Join(wl, "\",\""), strmangle.Placeholders(dialect.UseIndexPlaceholders, len(wl), 1, 1))
		} else {
			cache.query = "INSERT INTO \"matchtagrelationship\" () VALUES ()%s%s"
		}

		var queryOutput, queryReturning string

		if len(cache.retMapping) != 0 {
			cache.retQuery = fmt.Sprintf("SELECT \"%s\" FROM \"matchtagrelationship\" WHERE %s", strings.Join(returnColumns, "\",\""), strmangle.WhereClause("\"", "\"", 0, matchtagrelationshipPrimaryKeyColumns))
		}

		cache.query = fmt.Sprintf(cache.query, queryOutput, queryReturning)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	vals := queries.ValuesFromMapping(value, cache.valueMapping)

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, vals)
	}

	result, err := exec.ExecContext(ctx, cache.query, vals...)

	if err != nil {
		return errors.Wrap(err, "models: unable to insert into matchtagrelationship")
	}

	var lastID int64
	var identifierCols []interface{}

	if len(cache.retMapping) == 0 {
		goto CacheNoHooks
	}

	lastID, err = result.LastInsertId()
	if err != nil {
		return ErrSyncFail
	}

	o.ID = int64(lastID)
	if lastID != 0 && len(cache.retMapping) == 1 && cache.retMapping[0] == matchtagrelationshipMapping["ID"] {
		goto CacheNoHooks
	}

	identifierCols = []interface{}{
		o.ID,
	}

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.retQuery)
		fmt.Fprintln(boil.DebugWriter, identifierCols...)
	}

	err = exec.QueryRowContext(ctx, cache.retQuery, identifierCols...).Scan(queries.PtrsFromMapping(value, cache.retMapping)...)
	if err != nil {
		return errors.Wrap(err, "models: unable to populate default values for matchtagrelationship")
	}

CacheNoHooks:
	if !cached {
		matchtagrelationshipInsertCacheMut.Lock()
		matchtagrelationshipInsertCache[key] = cache
		matchtagrelationshipInsertCacheMut.Unlock()
	}

	return o.doAfterInsertHooks(ctx, exec)
}

// Update uses an executor to update the Matchtagrelationship.
// See boil.Columns.UpdateColumnSet documentation to understand column list inference for updates.
// Update does not automatically update the record in case of default values. Use .Reload() to refresh the records.
func (o *Matchtagrelationship) Update(ctx context.Context, exec boil.ContextExecutor, columns boil.Columns) (int64, error) {
	var err error
	if err = o.doBeforeUpdateHooks(ctx, exec); err != nil {
		return 0, err
	}
	key := makeCacheKey(columns, nil)
	matchtagrelationshipUpdateCacheMut.RLock()
	cache, cached := matchtagrelationshipUpdateCache[key]
	matchtagrelationshipUpdateCacheMut.RUnlock()

	if !cached {
		wl := columns.UpdateColumnSet(
			matchtagrelationshipAllColumns,
			matchtagrelationshipPrimaryKeyColumns,
		)

		if !columns.IsWhitelist() {
			wl = strmangle.SetComplement(wl, []string{"created_at"})
		}
		if len(wl) == 0 {
			return 0, errors.New("models: unable to update matchtagrelationship, could not build whitelist")
		}

		cache.query = fmt.Sprintf("UPDATE \"matchtagrelationship\" SET %s WHERE %s",
			strmangle.SetParamNames("\"", "\"", 0, wl),
			strmangle.WhereClause("\"", "\"", 0, matchtagrelationshipPrimaryKeyColumns),
		)
		cache.valueMapping, err = queries.BindMapping(matchtagrelationshipType, matchtagrelationshipMapping, append(wl, matchtagrelationshipPrimaryKeyColumns...))
		if err != nil {
			return 0, err
		}
	}

	values := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), cache.valueMapping)

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, cache.query)
		fmt.Fprintln(boil.DebugWriter, values)
	}

	var result sql.Result
	result, err = exec.ExecContext(ctx, cache.query, values...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update matchtagrelationship row")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by update for matchtagrelationship")
	}

	if !cached {
		matchtagrelationshipUpdateCacheMut.Lock()
		matchtagrelationshipUpdateCache[key] = cache
		matchtagrelationshipUpdateCacheMut.Unlock()
	}

	return rowsAff, o.doAfterUpdateHooks(ctx, exec)
}

// UpdateAll updates all rows with the specified column values.
func (q matchtagrelationshipQuery) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	queries.SetUpdate(q.Query, cols)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all for matchtagrelationship")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected for matchtagrelationship")
	}

	return rowsAff, nil
}

// UpdateAll updates all rows with the specified column values, using an executor.
func (o MatchtagrelationshipSlice) UpdateAll(ctx context.Context, exec boil.ContextExecutor, cols M) (int64, error) {
	ln := int64(len(o))
	if ln == 0 {
		return 0, nil
	}

	if len(cols) == 0 {
		return 0, errors.New("models: update all requires at least one column argument")
	}

	colNames := make([]string, len(cols))
	args := make([]interface{}, len(cols))

	i := 0
	for name, value := range cols {
		colNames[i] = name
		args[i] = value
		i++
	}

	// Append all of the primary key values for each column
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), matchtagrelationshipPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := fmt.Sprintf("UPDATE \"matchtagrelationship\" SET %s WHERE %s",
		strmangle.SetParamNames("\"", "\"", 0, colNames),
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 0, matchtagrelationshipPrimaryKeyColumns, len(o)))

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args...)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to update all in matchtagrelationship slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to retrieve rows affected all in update all matchtagrelationship")
	}
	return rowsAff, nil
}

// Delete deletes a single Matchtagrelationship record with an executor.
// Delete will match against the primary key column to find the record to delete.
func (o *Matchtagrelationship) Delete(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if o == nil {
		return 0, errors.New("models: no Matchtagrelationship provided for delete")
	}

	if err := o.doBeforeDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	args := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(o)), matchtagrelationshipPrimaryKeyMapping)
	sql := "DELETE FROM \"matchtagrelationship\" WHERE \"id\"=?"

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args...)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete from matchtagrelationship")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by delete for matchtagrelationship")
	}

	if err := o.doAfterDeleteHooks(ctx, exec); err != nil {
		return 0, err
	}

	return rowsAff, nil
}

// DeleteAll deletes all matching rows.
func (q matchtagrelationshipQuery) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if q.Query == nil {
		return 0, errors.New("models: no matchtagrelationshipQuery provided for delete all")
	}

	queries.SetDelete(q.Query)

	result, err := q.Query.ExecContext(ctx, exec)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from matchtagrelationship")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for matchtagrelationship")
	}

	return rowsAff, nil
}

// DeleteAll deletes all rows in the slice, using an executor.
func (o MatchtagrelationshipSlice) DeleteAll(ctx context.Context, exec boil.ContextExecutor) (int64, error) {
	if len(o) == 0 {
		return 0, nil
	}

	if len(matchtagrelationshipBeforeDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doBeforeDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	var args []interface{}
	for _, obj := range o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), matchtagrelationshipPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "DELETE FROM \"matchtagrelationship\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 0, matchtagrelationshipPrimaryKeyColumns, len(o))

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, args)
	}

	result, err := exec.ExecContext(ctx, sql, args...)
	if err != nil {
		return 0, errors.Wrap(err, "models: unable to delete all from matchtagrelationship slice")
	}

	rowsAff, err := result.RowsAffected()
	if err != nil {
		return 0, errors.Wrap(err, "models: failed to get rows affected by deleteall for matchtagrelationship")
	}

	if len(matchtagrelationshipAfterDeleteHooks) != 0 {
		for _, obj := range o {
			if err := obj.doAfterDeleteHooks(ctx, exec); err != nil {
				return 0, err
			}
		}
	}

	return rowsAff, nil
}

// Reload refetches the object from the database
// using the primary keys with an executor.
func (o *Matchtagrelationship) Reload(ctx context.Context, exec boil.ContextExecutor) error {
	ret, err := FindMatchtagrelationship(ctx, exec, o.ID)
	if err != nil {
		return err
	}

	*o = *ret
	return nil
}

// ReloadAll refetches every row with matching primary key column values
// and overwrites the original object slice with the newly updated slice.
func (o *MatchtagrelationshipSlice) ReloadAll(ctx context.Context, exec boil.ContextExecutor) error {
	if o == nil || len(*o) == 0 {
		return nil
	}

	slice := MatchtagrelationshipSlice{}
	var args []interface{}
	for _, obj := range *o {
		pkeyArgs := queries.ValuesFromMapping(reflect.Indirect(reflect.ValueOf(obj)), matchtagrelationshipPrimaryKeyMapping)
		args = append(args, pkeyArgs...)
	}

	sql := "SELECT \"matchtagrelationship\".* FROM \"matchtagrelationship\" WHERE " +
		strmangle.WhereClauseRepeated(string(dialect.LQ), string(dialect.RQ), 0, matchtagrelationshipPrimaryKeyColumns, len(*o))

	q := queries.Raw(sql, args...)

	err := q.Bind(ctx, exec, &slice)
	if err != nil {
		return errors.Wrap(err, "models: unable to reload all in MatchtagrelationshipSlice")
	}

	*o = slice

	return nil
}

// MatchtagrelationshipExists checks if the Matchtagrelationship row exists.
func MatchtagrelationshipExists(ctx context.Context, exec boil.ContextExecutor, iD int64) (bool, error) {
	var exists bool
	sql := "select exists(select 1 from \"matchtagrelationship\" where \"id\"=? limit 1)"

	if boil.DebugMode {
		fmt.Fprintln(boil.DebugWriter, sql)
		fmt.Fprintln(boil.DebugWriter, iD)
	}

	row := exec.QueryRowContext(ctx, sql, iD)

	err := row.Scan(&exists)
	if err != nil {
		return false, errors.Wrap(err, "models: unable to check if matchtagrelationship exists")
	}

	return exists, nil
}
