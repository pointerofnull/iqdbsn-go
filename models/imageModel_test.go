// Code generated by SQLBoiler (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"bytes"
	"context"
	"reflect"
	"testing"

	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries"
	"github.com/volatiletech/sqlboiler/randomize"
	"github.com/volatiletech/sqlboiler/strmangle"
)

var (
	// Relationships sometimes use the reflection helper queries.Equal/queries.Assign
	// so force a package dependency in case they don't.
	_ = queries.Equal
)

func testImagemodels(t *testing.T) {
	t.Parallel()

	query := Imagemodels()

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}

func testImagemodelsDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := o.Delete(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testImagemodelsQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := Imagemodels().DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testImagemodelsSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := ImagemodelSlice{o}

	if rowsAff, err := slice.DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testImagemodelsExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	e, err := ImagemodelExists(ctx, tx, o.ID)
	if err != nil {
		t.Errorf("Unable to check if Imagemodel exists: %s", err)
	}
	if !e {
		t.Errorf("Expected ImagemodelExists to return true, but got false.")
	}
}

func testImagemodelsFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	imagemodelFound, err := FindImagemodel(ctx, tx, o.ID)
	if err != nil {
		t.Error(err)
	}

	if imagemodelFound == nil {
		t.Error("want a record, got nil")
	}
}

func testImagemodelsBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = Imagemodels().Bind(ctx, tx, o); err != nil {
		t.Error(err)
	}
}

func testImagemodelsOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if x, err := Imagemodels().One(ctx, tx); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testImagemodelsAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	imagemodelOne := &Imagemodel{}
	imagemodelTwo := &Imagemodel{}
	if err = randomize.Struct(seed, imagemodelOne, imagemodelDBTypes, false, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}
	if err = randomize.Struct(seed, imagemodelTwo, imagemodelDBTypes, false, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = imagemodelOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = imagemodelTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := Imagemodels().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testImagemodelsCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	imagemodelOne := &Imagemodel{}
	imagemodelTwo := &Imagemodel{}
	if err = randomize.Struct(seed, imagemodelOne, imagemodelDBTypes, false, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}
	if err = randomize.Struct(seed, imagemodelTwo, imagemodelDBTypes, false, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = imagemodelOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = imagemodelTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}

func imagemodelBeforeInsertHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelAfterInsertHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelAfterSelectHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelBeforeUpdateHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelAfterUpdateHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelBeforeDeleteHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelAfterDeleteHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelBeforeUpsertHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func imagemodelAfterUpsertHook(ctx context.Context, e boil.ContextExecutor, o *Imagemodel) error {
	*o = Imagemodel{}
	return nil
}

func testImagemodelsHooks(t *testing.T) {
	t.Parallel()

	var err error

	ctx := context.Background()
	empty := &Imagemodel{}
	o := &Imagemodel{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, imagemodelDBTypes, false); err != nil {
		t.Errorf("Unable to randomize Imagemodel object: %s", err)
	}

	AddImagemodelHook(boil.BeforeInsertHook, imagemodelBeforeInsertHook)
	if err = o.doBeforeInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	imagemodelBeforeInsertHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.AfterInsertHook, imagemodelAfterInsertHook)
	if err = o.doAfterInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	imagemodelAfterInsertHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.AfterSelectHook, imagemodelAfterSelectHook)
	if err = o.doAfterSelectHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	imagemodelAfterSelectHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.BeforeUpdateHook, imagemodelBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	imagemodelBeforeUpdateHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.AfterUpdateHook, imagemodelAfterUpdateHook)
	if err = o.doAfterUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	imagemodelAfterUpdateHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.BeforeDeleteHook, imagemodelBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	imagemodelBeforeDeleteHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.AfterDeleteHook, imagemodelAfterDeleteHook)
	if err = o.doAfterDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	imagemodelAfterDeleteHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.BeforeUpsertHook, imagemodelBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	imagemodelBeforeUpsertHooks = []ImagemodelHook{}

	AddImagemodelHook(boil.AfterUpsertHook, imagemodelAfterUpsertHook)
	if err = o.doAfterUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	imagemodelAfterUpsertHooks = []ImagemodelHook{}
}

func testImagemodelsInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testImagemodelsInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Whitelist(imagemodelColumnsWithoutDefault...)); err != nil {
		t.Error(err)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testImagemodelToManyImageImagematchrelationships(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Imagemodel
	var b, c Imagematchrelationship

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, imagematchrelationshipDBTypes, false, imagematchrelationshipColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, imagematchrelationshipDBTypes, false, imagematchrelationshipColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.ImageID = a.ID
	c.ImageID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.ImageImagematchrelationships().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if v.ImageID == b.ImageID {
			bFound = true
		}
		if v.ImageID == c.ImageID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := ImagemodelSlice{&a}
	if err = a.L.LoadImageImagematchrelationships(ctx, tx, false, (*[]*Imagemodel)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.ImageImagematchrelationships); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.ImageImagematchrelationships = nil
	if err = a.L.LoadImageImagematchrelationships(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.ImageImagematchrelationships); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testImagemodelToManyThumbnailThumbnailrelationships(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Imagemodel
	var b, c Thumbnailrelationship

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, thumbnailrelationshipDBTypes, false, thumbnailrelationshipColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, thumbnailrelationshipDBTypes, false, thumbnailrelationshipColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.ThumbnailID = a.ID
	c.ThumbnailID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.ThumbnailThumbnailrelationships().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if v.ThumbnailID == b.ThumbnailID {
			bFound = true
		}
		if v.ThumbnailID == c.ThumbnailID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := ImagemodelSlice{&a}
	if err = a.L.LoadThumbnailThumbnailrelationships(ctx, tx, false, (*[]*Imagemodel)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.ThumbnailThumbnailrelationships); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.ThumbnailThumbnailrelationships = nil
	if err = a.L.LoadThumbnailThumbnailrelationships(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.ThumbnailThumbnailrelationships); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testImagemodelToManyOriginalThumbnailrelationships(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Imagemodel
	var b, c Thumbnailrelationship

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, thumbnailrelationshipDBTypes, false, thumbnailrelationshipColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, thumbnailrelationshipDBTypes, false, thumbnailrelationshipColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.OriginalID = a.ID
	c.OriginalID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.OriginalThumbnailrelationships().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if v.OriginalID == b.OriginalID {
			bFound = true
		}
		if v.OriginalID == c.OriginalID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := ImagemodelSlice{&a}
	if err = a.L.LoadOriginalThumbnailrelationships(ctx, tx, false, (*[]*Imagemodel)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.OriginalThumbnailrelationships); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.OriginalThumbnailrelationships = nil
	if err = a.L.LoadOriginalThumbnailrelationships(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.OriginalThumbnailrelationships); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testImagemodelToManyAddOpImageImagematchrelationships(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Imagemodel
	var b, c, d, e Imagematchrelationship

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, imagemodelDBTypes, false, strmangle.SetComplement(imagemodelPrimaryKeyColumns, imagemodelColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Imagematchrelationship{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, imagematchrelationshipDBTypes, false, strmangle.SetComplement(imagematchrelationshipPrimaryKeyColumns, imagematchrelationshipColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*Imagematchrelationship{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddImageImagematchrelationships(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.ImageID {
			t.Error("foreign key was wrong value", a.ID, first.ImageID)
		}
		if a.ID != second.ImageID {
			t.Error("foreign key was wrong value", a.ID, second.ImageID)
		}

		if first.R.Image != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.Image != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.ImageImagematchrelationships[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.ImageImagematchrelationships[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.ImageImagematchrelationships().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}
func testImagemodelToManyAddOpThumbnailThumbnailrelationships(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Imagemodel
	var b, c, d, e Thumbnailrelationship

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, imagemodelDBTypes, false, strmangle.SetComplement(imagemodelPrimaryKeyColumns, imagemodelColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Thumbnailrelationship{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, thumbnailrelationshipDBTypes, false, strmangle.SetComplement(thumbnailrelationshipPrimaryKeyColumns, thumbnailrelationshipColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*Thumbnailrelationship{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddThumbnailThumbnailrelationships(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.ThumbnailID {
			t.Error("foreign key was wrong value", a.ID, first.ThumbnailID)
		}
		if a.ID != second.ThumbnailID {
			t.Error("foreign key was wrong value", a.ID, second.ThumbnailID)
		}

		if first.R.Thumbnail != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.Thumbnail != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.ThumbnailThumbnailrelationships[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.ThumbnailThumbnailrelationships[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.ThumbnailThumbnailrelationships().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}
func testImagemodelToManyAddOpOriginalThumbnailrelationships(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Imagemodel
	var b, c, d, e Thumbnailrelationship

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, imagemodelDBTypes, false, strmangle.SetComplement(imagemodelPrimaryKeyColumns, imagemodelColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Thumbnailrelationship{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, thumbnailrelationshipDBTypes, false, strmangle.SetComplement(thumbnailrelationshipPrimaryKeyColumns, thumbnailrelationshipColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*Thumbnailrelationship{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddOriginalThumbnailrelationships(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.OriginalID {
			t.Error("foreign key was wrong value", a.ID, first.OriginalID)
		}
		if a.ID != second.OriginalID {
			t.Error("foreign key was wrong value", a.ID, second.OriginalID)
		}

		if first.R.Original != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.Original != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.OriginalThumbnailrelationships[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.OriginalThumbnailrelationships[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.OriginalThumbnailrelationships().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}

func testImagemodelsReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = o.Reload(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testImagemodelsReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := ImagemodelSlice{o}

	if err = slice.ReloadAll(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testImagemodelsSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := Imagemodels().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	imagemodelDBTypes = map[string]string{`ID`: `INTEGER`, `Checksum`: `VARCHAR(255)`, `Width`: `INTEGER`, `Height`: `INTEGER`, `Path`: `VARCHAR(255)`}
	_                 = bytes.MinRead
)

func testImagemodelsUpdate(t *testing.T) {
	t.Parallel()

	if 0 == len(imagemodelPrimaryKeyColumns) {
		t.Skip("Skipping table with no primary key columns")
	}
	if len(imagemodelAllColumns) == len(imagemodelPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	if rowsAff, err := o.Update(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only affect one row but affected", rowsAff)
	}
}

func testImagemodelsSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(imagemodelAllColumns) == len(imagemodelPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &Imagemodel{}
	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Imagemodels().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, imagemodelDBTypes, true, imagemodelPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Imagemodel struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(imagemodelAllColumns, imagemodelPrimaryKeyColumns) {
		fields = imagemodelAllColumns
	} else {
		fields = strmangle.SetComplement(
			imagemodelAllColumns,
			imagemodelPrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	typ := reflect.TypeOf(o).Elem()
	n := typ.NumField()

	updateMap := M{}
	for _, col := range fields {
		for i := 0; i < n; i++ {
			f := typ.Field(i)
			if f.Tag.Get("boil") == col {
				updateMap[col] = value.Field(i).Interface()
			}
		}
	}

	slice := ImagemodelSlice{o}
	if rowsAff, err := slice.UpdateAll(ctx, tx, updateMap); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("wanted one record updated but got", rowsAff)
	}
}
