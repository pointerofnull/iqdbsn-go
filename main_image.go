package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/disintegration/imaging"
	"github.com/steakknife/hamming"
)

//resizeImage resizes original image for upload to iqdb/saucenao
func resizeImage(file string) string {
	cwd, _ := os.Getwd()
	fileHash, _ := hashFileSHA256(file)
	src, err := imaging.Open(file)
	if err != nil {
		log.Fatalf("failed to open image: %v", err)
	}
	src = imaging.Resize(src, 150, 0, imaging.Lanczos)
	err = imaging.Save(src, filepath.FromSlash(fmt.Sprintf("%s/%s/%s/%s.png", cwd, iqdbgoDir, thumbnailPath, fileHash)))
	if err != nil {
		log.Fatalf("failed to save image: %v", err)
	}
	fmt.Printf("ThumbnailPath: %+v\n", fmt.Sprintf("%s/%s/%s/%s.png", cwd, iqdbgoDir, thumbnailPath, fileHash))
	return filepath.FromSlash(fmt.Sprintf("%s/%s/%s/%s.png", cwd, iqdbgoDir, thumbnailPath, fileHash))
}

//imageHammingCompare does a hamming comparative between two hashes in []byte form
//values greater than 0 indicate more difference between hashes
//@param hashOne []byte
//@param hashTwo []byte
//@return int
func imageHammingCompare(hashOne []byte, hashTwo []byte) int {
	return hamming.Bytes(hashOne, hashTwo)
}
