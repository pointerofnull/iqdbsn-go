BEGIN TRANSACTION;
PRAGMA foreign_keys = '1';
DROP TABLE IF EXISTS "thumbnailrelationship";
CREATE TABLE IF NOT EXISTS "thumbnailrelationship" (
	"id"	INTEGER NOT NULL,
	"original_id"	INTEGER NOT NULL,
	"thumbnail_id"	INTEGER NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY("original_id") REFERENCES "imagemodel"("id"),
	FOREIGN KEY("thumbnail_id") REFERENCES "imagemodel"("id")
);
DROP TABLE IF EXISTS "program";
CREATE TABLE IF NOT EXISTS "program" (
	"id"	INTEGER NOT NULL,
	"version"	INTEGER NOT NULL,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "matchtagrelationship";
CREATE TABLE IF NOT EXISTS "matchtagrelationship" (
	"id"	INTEGER NOT NULL,
	"match_id"	INTEGER NOT NULL,
	"tag_id"	INTEGER NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY("match_id") REFERENCES "match"("id"),
	FOREIGN KEY("tag_id") REFERENCES "tag"("id")
);
DROP TABLE IF EXISTS "tag";
CREATE TABLE IF NOT EXISTS "tag" (
	"id"	INTEGER NOT NULL,
	"name"	VARCHAR(255) NOT NULL,
	"namespace"	VARCHAR(255),
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "imagematch";
CREATE TABLE IF NOT EXISTS "imagematch" (
	"id"	INTEGER NOT NULL,
	"match_id"	INTEGER NOT NULL,
	"similarity"	INTEGER NOT NULL,
	"status"	INTEGER NOT NULL,
	"search_place"	INTEGER NOT NULL,
	"created_date"	DATETIME NOT NULL,
	"force_gray"	INTEGER NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY("match_id") REFERENCES "imagematchrelationship"("id")
);
DROP TABLE IF EXISTS "imagematchrelationship";
CREATE TABLE IF NOT EXISTS "imagematchrelationship" (
	"id"	INTEGER NOT NULL,
	"image_id"	INTEGER NOT NULL,
	"match_result_id"	INTEGER NOT NULL,
	FOREIGN KEY("match_result_id") REFERENCES "match"("id"),
	PRIMARY KEY("id"),
	FOREIGN KEY("image_id") REFERENCES "imagemodel"("id")
);
DROP TABLE IF EXISTS "match";
CREATE TABLE IF NOT EXISTS "match" (
	"id"	INTEGER NOT NULL,
	"href"	VARCHAR(255) NOT NULL,
	"thumb"	VARCHAR(255) NOT NULL,
	"rating"	VARCHAR(255) NOT NULL,
	"img_alt"	TEXT,
	"width"	INTEGER,
	"height"	INTEGER,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "imagemodel";
CREATE TABLE IF NOT EXISTS "imagemodel" (
	"id"	INTEGER NOT NULL,
	"checksum"	VARCHAR(255) NOT NULL,
	"width"	INTEGER NOT NULL,
	"height"	INTEGER NOT NULL,
	"path"	VARCHAR(255),
	PRIMARY KEY("id")
);
DROP INDEX IF EXISTS "thumbnailrelationship_thumbnail_id";
CREATE INDEX IF NOT EXISTS "thumbnailrelationship_thumbnail_id" ON "thumbnailrelationship" (
	"thumbnail_id"
);
DROP INDEX IF EXISTS "thumbnailrelationship_original_id";
CREATE INDEX IF NOT EXISTS "thumbnailrelationship_original_id" ON "thumbnailrelationship" (
	"original_id"
);
DROP INDEX IF EXISTS "matchtagrelationship_tag_id";
CREATE INDEX IF NOT EXISTS "matchtagrelationship_tag_id" ON "matchtagrelationship" (
	"tag_id"
);
DROP INDEX IF EXISTS "matchtagrelationship_match_id";
CREATE INDEX IF NOT EXISTS "matchtagrelationship_match_id" ON "matchtagrelationship" (
	"match_id"
);
DROP INDEX IF EXISTS "imagematch_match_id";
CREATE INDEX IF NOT EXISTS "imagematch_match_id" ON "imagematch" (
	"match_id"
);
DROP INDEX IF EXISTS "imagematchrelationship_match_result_id";
CREATE INDEX IF NOT EXISTS "imagematchrelationship_match_result_id" ON "imagematchrelationship" (
	"match_result_id"
);
DROP INDEX IF EXISTS "imagematchrelationship_image_id";
CREATE INDEX IF NOT EXISTS "imagematchrelationship_image_id" ON "imagematchrelationship" (
	"image_id"
);
DROP INDEX IF EXISTS "match_href";
CREATE UNIQUE INDEX IF NOT EXISTS "match_href" ON "match" (
	"href"
);
DROP INDEX IF EXISTS "imagemodel_checksum";
CREATE UNIQUE INDEX IF NOT EXISTS "imagemodel_checksum" ON "imagemodel" (
	"checksum"
);
COMMIT;