BEGIN TRANSACTION;
PRAGMA foreign_keys = '1';
DROP TABLE IF EXISTS "tag";
CREATE TABLE IF NOT EXISTS "tag" (
	"id"		integer NOT NULL PRIMARY KEY UNIQUE,
	"tag"		varchar(255) NOT NULL UNIQUE,
	"namespace"	varchar(255) NOT NULL
);
DROP TABLE IF EXISTS "match";
CREATE TABLE IF NOT EXISTS "match" (
	"id"		integer NOT NULL PRIMARY KEY UNIQUE,
	"href"		varchar(512) NOT NULL UNIQUE, 
	"thumb"		varchar(512) NOT NULL, 
	"rating"	varchar(255) NOT NULL,
	"img_alt"	text NOT NULL,
	"width"		int NOT NULL,
	"height"	int NOT NULL,
	"extra_href" varchar(512)
);
DROP TABLE IF EXISTS "thumbnailModel";
CREATE TABLE IF NOT EXISTS "thumbnailModel" (
	"id"		integer NOT NULL PRIMARY KEY UNIQUE,
	"hash"	varchar(255) NOT NULL UNIQUE,
	"width"		int NOT NULL,
	"height"	int NOT NULL,
	"path"		varchar(512) NOT NULL,
	"a_hash"		varchar(255) NOT NULL,
	"d_hash"		varchar(255) NOT NULL
);
DROP TABLE IF EXISTS "imageModel";
CREATE TABLE IF NOT EXISTS "imageModel" (
	"id"		integer NOT NULL PRIMARY KEY UNIQUE,
	"hash"	varchar(255) NOT NULL UNIQUE,
	"width"		int NOT NULL,
	"height"	int NOT NULL,
	"path"		varchar(512) NOT NULL,
	"a_hash"		varchar(255) NOT NULL,
	"d_hash"		varchar(255) NOT NULL
);
DROP TABLE IF EXISTS "thumbnailRelationship";
CREATE TABLE IF NOT EXISTS "thumbnailRelationship" (
	"id"		integer NOT NULL PRIMARY KEY UNIQUE,
	"orig_id"	integer NOT NULL UNIQUE,
	"thumb_id"	integer NOT NULL UNIQUE
);
DROP TABLE IF EXISTS "imageMatchRelationship";
CREATE TABLE IF NOT EXISTS "imageMatchRelationship" (
	"id"			integer NOT NULL PRIMARY KEY UNIQUE,
	"image_id"		integer NOT NULL,
	"match_result_id"	integer NOT NULL
);
DROP TABLE IF EXISTS "imageMatch";
CREATE TABLE IF NOT EXISTS "imageMatch" (
	"id"			integer NOT NULL PRIMARY KEY UNIQUE,
	"match_id"		integer NOT NULL UNIQUE,
	"similarity"	integer NOT NULL,
	"status"		integer,
	"search_place"	integer,
	"created_at"		datetime NOT NULL,
	"force_gray"		integer
);
DROP TABLE IF EXISTS "program";
CREATE TABLE IF NOT EXISTS "program" (
	"id"		integer NOT NULL PRIMARY KEY,
	"version"	integer NOT NULL
);
DROP TABLE IF EXISTS "matchTagRelationship";
CREATE TABLE IF NOT EXISTS "matchTagRelationship" (
	"id"		integer NOT NULL PRIMARY KEY UNIQUE,
	"match_id"	integer NOT NULL UNIQUE,
	"tag_id"		integer NOT NULL UNIQUE
);
DROP TABLE IF EXISTS "hashRelationship";
CREATE TABLE IF NOT EXISTS "hashRelationship" (
	"id"				integer NOT NULL PRIMARY KEY UNIQUE,
	"orig_id"			integer UNIQUE,
	"orig_a_hash"			varchar(255),
	"orig_d_hash"			varchar(255),
	"thumb_id"			integer UNIQUE,
	"thumb_a_hash"		varchar(255),
	"thumb_d_hash"		varchar(255),
	"hamming_a_hash"		integer NOT NULL,
	"hamming_d_hash"		integer NOT NULL,
	"orig_hamming"		varchar(255),
	"thumbnail_hamming"		varchar(255)
);
DROP INDEX IF EXISTS "thumbnailRelationshipOrigID";
CREATE INDEX IF NOT EXISTS "thumbnailRelationshipOrigID" ON "thumbnailRelationship" (
	"orig_id"
);
DROP INDEX IF EXISTS "thumbnailRelationshipThumbID";
CREATE INDEX IF NOT EXISTS "thumbnailRelationshipThumbID" ON "thumbnailRelationship" (
	"thumb_id"
);
DROP INDEX IF EXISTS "matchTagRelationshipTagID";
CREATE INDEX IF NOT EXISTS "matchTagRelationshipTagID" ON "matchTagRelationship" (
	"tag_id"
);
DROP INDEX IF EXISTS "matchTagRelationshipMatchID";
CREATE INDEX IF NOT EXISTS "matchTagRelationshipMatchID" ON "matchTagRelationship" (
	"match_id"
);
DROP INDEX IF EXISTS "matchHREF";
CREATE INDEX IF NOT EXISTS "matchHREF" ON "match" (
	"href"
);
DROP INDEX IF EXISTS "thumbnailModelDHash";
CREATE INDEX IF NOT EXISTS "thumbnailModelDHash" ON "thumbnailModel" (
	"d_hash"
);
DROP INDEX IF EXISTS "thumbnailModelAHash";
CREATE INDEX IF NOT EXISTS "thumbnailModelAHash" ON "thumbnailModel" (
	"a_hash"
)
DROP INDEX IF EXISTS "thumbnailModelSHA256";
CREATE INDEX IF NOT EXISTS "thumbnailModelSHA256" ON "thumbnailModel" (
	"hash"
);
DROP INDEX IF EXISTS "imageModelDHash";
CREATE INDEX IF NOT EXISTS "imageModelDHash" ON "imageModel" (
	"d_hash"
);
DROP INDEX IF EXISTS "imageModelAHash";
CREATE INDEX IF NOT EXISTS "imageModelAHash" ON "imageModel" (
	"a_hash"
);
DROP INDEX IF EXISTS "imageModelSHA256";
CREATE INDEX IF NOT EXISTS "imageModelSHA256" ON "imageModel" (
	"hash"
);
DROP INDEX IF EXISTS "imageMatchRelationshipMatchResultID";
CREATE INDEX IF NOT EXISTS "imageMatchRelationshipMatchResultID" ON "imageMatchRelationship" (
	"match_result_id"
);
DROP INDEX IF EXISTS "imageMatchRelationshipImageID";
CREATE INDEX IF NOT EXISTS "imageMatchRelationshipImageID" ON "imageMatchRelationship" (
	"image_id"
);
DROP INDEX IF EXISTS "imageMatchMatchID";
CREATE INDEX IF NOT EXISTS "imageMatchMatchID" ON "imageMatch" (
	"match_id"
);
COMMIT;