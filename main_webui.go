package main

import (
	"fmt"

	"github.com/kataras/iris"
)

const (
	webuiPort = ":5007"
)

func startWEBUI() {
	router := iris.Default()
	router.StaticWeb("/static", "./webui/static")
	tmpl := iris.HTML("./webui/templates", ".html")
	tmpl.Reload(true)
	router.RegisterView(tmpl)
	router.Get("/", func(ctx iris.Context) {
		ctx.ViewData("title", "webui for iqdb-go")
		ctx.ViewData("appName", fmt.Sprintf("%s v%s", applicationName, applicationVersion))
		ctx.View("index.html")
	})
	router.Get("/matchview/d/{matchid:uint32}", matchViewByID)
	router.Get("/matchview/hash/{hash:string}", matchViewByHash)
	router.Get("/matchview/dhash/{dhash:string}", matchViewByDHash)
	errRouter := router.Run(iris.Addr(webuiPort))
	if errRouter != nil {
		fmt.Printf("Error binding port: %+v\n", errRouter)
	}
}

func matchViewByID(ctx iris.Context) {
	matchID, err := ctx.Params().GetUint32("matchid")
	if err == nil {
		matchInfo := returnMatchByID(matchID)
		ctx.ViewData("cardIMG", matchInfo.Thumbnail)
		ctx.ViewData("cardIMGALT", matchInfo.ImgAlt)
		ctx.ViewData("imgWidth", matchInfo.ImageWidth)
		ctx.ViewData("imgHeight", matchInfo.ImageHeight)
		ctx.ViewData("imgRating", matchInfo.Rating)
		ctx.ViewData("cardHREF", matchInfo.MatchURL)
		ctx.View("matchview.html")
		//ctx.Writef("Match ID: %+v", matchID)
	} else {
		ctx.NotFound()
	}
}

func matchViewByHash(ctx iris.Context) {
	fileHash := ctx.Params().GetStringTrim("hash")
	if fileHash != "" {
		ctx.Writef("FileHash: %+v", fileHash)
		ctx.View("matchview.html")
	} else {
		ctx.NotFound()
	}

}

func matchViewByDHash(ctx iris.Context) {
	fileHash := ctx.Params().GetStringTrim("dhash")
	if fileHash != "" {
		ctx.Writef("FileHash: %+v", fileHash)
		ctx.View("matchview.html")
	} else {
		ctx.NotFound()
	}

}
